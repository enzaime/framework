<?php

if (!defined('ENZAIME_USERS')) {
    define('ENZAIME_USERS', 'enzaime-users');
}

if (!defined('ADMIN_USER_LOGGED_IN')) {
    define('ADMIN_USER_LOGGED_IN', 'admin-user-logged-in');
}

if (!defined('ADMIN_USER_BEFORE_LOGGED_OUT')) {
    define('ADMIN_USER_BEFORE_LOGGED_OUT', 'admin-user-before-logged-out');
}


if (!defined('ADMIN_USER_LOGGED_OUT')) {
    define('ADMIN_USER_LOGGED_OUT', 'admin-user-logged-out');
}

if (!defined('BASE_ACTION_AFTER_REGISTER_USER')) {
    define('BASE_ACTION_AFTER_REGISTER_USER', 'admin_user_registered');
}