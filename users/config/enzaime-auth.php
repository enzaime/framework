<?php

return [
    'login_using' => 'email',
    'guard' => 'admin',
    'front_actions' => [
        'guard' => 'web',
        'login' => [
            'view' => 'enzaime-theme::front.auth.login',
            'controller' => Enzaime\Base\Users\Http\Controllers\Front\AuthFrontController::class,
        ],
        'register' => [
            'view' => 'enzaime-theme::front.auth.register',
            'controller' => Enzaime\Base\Users\Http\Controllers\Front\RegisterController::class,
        ],
        'forgot_password' => [
            'view' => 'enzaime-theme::front.auth.forgot-password',
            'controller' => Enzaime\Base\Users\Http\Controllers\Front\ForgotPasswordController::class,
            'email_template' => ENZAIME_USERS . '::front.emails.forgot-password',
            /**
             * The unit is in minute. Only accept integer.
             */
            'link_expired_after' => 60,
        ],
        'reset_password' => [
            'view' => 'enzaime-theme::front.auth.reset-password',
            'controller' => Enzaime\Base\Users\Http\Controllers\Front\ResetPasswordController::class,
            'auto_sign_in_after_reset' => true,
            'remember_login' => true,
        ],
        'social_login' => [
            'controller' => Enzaime\Base\Users\Http\Controllers\Front\SocialAuthController::class,
        ],
    ],
];
