<?php namespace Enzaime\Base\Users\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Users\Http\Middleware\AuthenticateAdmin;
use Enzaime\Base\Users\Http\Middleware\AuthenticateFront;
use Enzaime\Base\Users\Http\Middleware\GuestAdmin;
use Enzaime\Base\Users\Http\Middleware\GuestFront;

class MiddlewareServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * @var Router $router
         */
        $router = $this->app['router'];

        $router->aliasMiddleware('enzaime.auth-admin', AuthenticateAdmin::class);
        $router->aliasMiddleware('enzaime.guest-admin', GuestAdmin::class);

        $router->aliasMiddleware('enzaime.auth-front', AuthenticateFront::class);
        $router->aliasMiddleware('enzaime.guest-front', GuestFront::class);

        if (is_admin_panel()) {
            $router->pushMiddlewareToGroup('web', AuthenticateAdmin::class);
        }
    }
}
