<?php namespace Enzaime\Base\Users\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Users\Models\PasswordReset;
use Enzaime\Base\Users\Models\User;
use Enzaime\Base\Users\Repositories\Contracts\PasswordResetRepositoryContract;
use Enzaime\Base\Users\Repositories\PasswordResetRepository;
use Enzaime\Base\Users\Repositories\UserRepository;
use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryContract::class, function () {
            return new UserRepository(new User());
        });

        $this->app->bind(PasswordResetRepositoryContract::class, function () {
            return new PasswordResetRepository(new PasswordReset());
        });
    }
}
