<?php namespace Enzaime\Base\Users\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Users\Hook\RegisterDashboardStats;
use Enzaime\Base\Users\Hook\AdminUserRegistrationHook;

class HookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        add_action(ENZAIME_DASHBOARD_STATS, [RegisterDashboardStats::class, 'handle'], 24);
        add_action(BASE_ACTION_AFTER_REGISTER_USER, [AdminUserRegistrationHook::class, 'handle'], 25);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
