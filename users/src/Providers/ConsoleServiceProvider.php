<?php namespace Enzaime\Base\Users\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Users\Console\Commands\CreateSuperAdminUserCommand;
use Enzaime\Base\Users\Console\Commands\ResetPasswordCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            ResetPasswordCommand::class,
            CreateSuperAdminUserCommand::class,
        ]);
    }
}
