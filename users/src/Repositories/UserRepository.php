<?php namespace Enzaime\Base\Users\Repositories;

use Illuminate\Support\Collection;
use Enzaime\Base\Models\EloquentBase;
use Enzaime\Base\Repositories\Eloquent\Traits\EloquentUseSoftDeletes;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;
use Enzaime\Base\Users\Models\Contracts\UserModelContract;
use Enzaime\Base\Users\Models\User;
use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;

class UserRepository extends EloquentBaseRepository implements UserRepositoryContract
{
    use EloquentUseSoftDeletes;

    /**
     * @param User|EloquentBase|int $user
     * @param array $data
     */
    public function syncRoles($user, array $data)
    {
        if (!$user instanceof UserModelContract) {
            $user = $this->find($user);
        }
        try {
            $data = array_fill_keys($data,  ['subdomain_id' => get_subdomain_id()]);
            $user->roles()->sync($data);
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param User|EloquentBase|int $user
     * @return Collection
     */
    public function getRoles($user)
    {
        if (!$user instanceof User) {
            $user = $this->find($user);
        }
        if ($user) {
            return $user->roles()->get();
        }
        return collect([]);
    }

    /**
     * @param User|EloquentBase $user
     * @return array
     */
    public function getRelatedRoleIds($user)
    {
        if ($user) {
            return $user->roles()->allRelatedIds()->toArray();
        }
        return [];
    }

    /**
     * @param array $data
     * @param array|null $roles
     * @return int|null|\Enzaime\Base\Models\EloquentBase
     */
    public function createUser(array $data, $roles = null)
    {
        $user = $this->create($data);
        if ($user && is_array($roles)) {
            $this->syncRoles($user, $roles);
        }
        return $user;
    }

    /**
     * @param int|User|EloquentBase $id
     * @param array $data
     * @param array|null $roles
     * @return int|null|\Enzaime\Base\Models\EloquentBase
     */
    public function updateUser($id, array $data, $roles = null)
    {
        $resultEditObject = $this->update($id, $data);

        if (!$resultEditObject) {
            return $resultEditObject;
        }

        if (is_array($roles)) {
            $this->syncRoles($id, $roles);
        }

        return $resultEditObject;
    }

    /**
     * @param User|EloquentBase|int $user
     * @return bool
     */
    public function isSuperAdmin($user)
    {
        if (!$user instanceof UserModelContract) {
            $user = $this->find($user);
        }

        if (!$user || !$user->isSuperAdmin()) {
            return false;
        }

        return true;
    }

    /**
     * @param User|EloquentBase|int $user
     * @param array $permissions
     * @return bool
     */
    public function hasPermission($user, array $permissions)
    {
        if (!$user instanceof User) {
            $user = $this->find($user);
        }

        if (!$user || !$user->hasPermission($permissions)) {
            return false;
        }

        return true;
    }

    /**
     * @param User|EloquentBase|int $user
     * @param array $roles
     * @return bool
     */
    public function hasRole($user, array $roles)
    {
        if (!$user instanceof UserModelContract) {
            $user = $this->find($user);
        }

        if (!$user || !$user->hasRole($roles)) {
            return false;
        }

        return true;
    }

     /**
     * @param boolean $onlyEnabled
     *
     * @return collection
     */
    public function getPlugins($user, $onlyEnabled = false) 
    {
        if (!$user instanceof UserModelContract) {
            $user = $this->find($user);
        }

        if (!$user || !count($user->plugins)) {
            return collect([]);
        }

        if ($onlyEnabled) {
            return $user->plugins()->wherePivot('enabled', true)->get();
        }

        return $user->plugins;
    }

      /**
     * @param User|int $user
     * @param int $pluginId
     * @param boolean $isEnabled
     * @return bool
     */
    public function setPlugin($user, $pluginId, $isEnabled)
    {
        if (!$user instanceof UserModelContract) {
            $user = $this->find($user);
        }

        return $user->plugins()
            ->syncWithoutDetaching([$pluginId => ['enabled' => $isEnabled]]);
    }

     /**
     * Find user by subdomain
     *
     * @param string $name
     * @return User
     */
    public function findBySubdomain($name) 
    {
        if (!$name) {
            return null;
        }
        return $this->findWhere(['username' => $name]);
    }
}
