<?php namespace Enzaime\Base\Users\Repositories;

use Carbon\Carbon;
use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;

use Enzaime\Base\Users\Repositories\Contracts\PasswordResetRepositoryContract;

class PasswordResetRepository extends EloquentBaseRepository implements PasswordResetRepositoryContract
{
    /**
     * @param array $data
     * @return int
     */
    public function createPasswordReset(array $data)
    {
        return $this->create($data);
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function createOrUpdatePasswordReset($id, array $data)
    {
        return $this->createOrUpdate($id, $data);
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function updatePasswordReset($id, array $data)
    {
        return $this->update($id, $data);
    }

    /**
     * @param int|BaseModelContract|array $id
     * @param bool $force
     * @return bool
     */
    public function deletePasswordReset($id, $force = false)
    {
        return $this->delete($id, $force);
    }

    /**
     * @param $token
     * @return \Illuminate\Database\Eloquent\Builder|mixed|null|\Enzaime\Base\Models\EloquentBase
     */
    public function getPasswordResetByToken($token)
    {
        return $this->findWhere([
            'token' => $token,
            ['expired_at', '>=', Carbon::now()],
        ]);
    }
}
