<?php namespace Enzaime\Base\Users\Models;

use Enzaime\Base\Users\Models\Contracts\PasswordResetModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class PasswordReset extends BaseModel implements PasswordResetModelContract
{
    protected $table = 'password_resets';

    protected $primaryKey = 'id';

    protected $fillable = ['email', 'token', 'expired_at'];

    public $timestamps = true;
    public $subdomain = false;
    
}
