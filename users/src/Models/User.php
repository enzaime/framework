<?php namespace Enzaime\Base\Users\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Enzaime\Subdomain\Models\Traits\Themable;
use Enzaime\Subdomain\Models\Traits\Pluginable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Enzaime\Base\Models\EloquentBase as BaseModel;
use Enzaime\Base\ModulesManagement\Models\Plugins;
use Enzaime\Base\ACL\Models\Traits\UserAuthorizable;
use Enzaime\Base\Users\Models\Contracts\UserModelContract;
use Enzaime\Base\Users\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements UserModelContract, AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    use UserAuthorizable;

    use SoftDeletes;

    use Notifiable;

    use Pluginable;

    use Themable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = [
        'username', 'email', 'password', 'name', 'user_type', 'avatar'
    ];

    /**
     * @return mixed|string
     */
    public function getUserName()
    {
        if ($this->display_name) {
            return $this->display_name;
        }
        return (($this->first_name ? $this->first_name . ' ' : '') . ($this->last_name ?: ''));
    }

    /**
     * @param $value
     * @return int
     */
    public function getIdAttribute($value)
    {
        return (int)$value;
    }

    /**
     * Hash the password before save to database
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = str_slug($value, '_');
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $expiredDateConfig = (int)config('enzaime-auth.front_actions.forgot_password.link_expired_after', 30) ?: 1;

        $expiredDate = Carbon::now()->addHour($expiredDateConfig);

        $data = [
            'username' => $this->username,
            'name' => $this->getUserName(),
            'email' => $this->email,
            'link' => route('front::auth.reset_password.get', [
                'token' => $token,
            ]),
            'token' => $token,
            'expired_at' => $expiredDate,
        ];

        $this->notify(new ResetPasswordNotification($data));
    }

    public function hasSubdomain()
    {
        return false;
    }

}
