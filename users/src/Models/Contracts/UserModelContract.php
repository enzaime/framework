<?php namespace Enzaime\Base\Users\Models\Contracts;

interface UserModelContract
{
    /**
     * @return string
     */
    public function getUserName();
}
