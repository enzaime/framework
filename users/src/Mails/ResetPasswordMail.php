<?php namespace Enzaime\Base\Users\Mails;

use Enzaime\Base\Mails\BaseMail;

class ResetPasswordMail extends BaseMail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(config('enzaime-auth.front_actions.forgot_password.email_template'));
    }
}
