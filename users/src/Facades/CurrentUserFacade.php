<?php namespace Enzaime\Base\Users\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Users\Support\CurrentUserSupport;

class CurrentUserFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CurrentUserSupport::class;
    }
}
