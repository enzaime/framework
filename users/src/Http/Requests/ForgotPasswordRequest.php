<?php namespace Enzaime\Base\Users\Http\Requests;

use Enzaime\Base\Http\Requests\Request;

class ForgotPasswordRequest extends Request
{
    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }
}
