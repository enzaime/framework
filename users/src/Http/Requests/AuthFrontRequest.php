<?php namespace Enzaime\Base\Users\Http\Requests;

use Enzaime\Base\Http\Requests\Request;

class AuthFrontRequest extends Request
{
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required',
        ];
    }
}
