<?php namespace Enzaime\Base\Users\Http\Controllers\Front;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Enzaime\Base\Http\Controllers\BaseFrontController;
use Enzaime\Base\Users\Http\Requests\ForgotPasswordRequest;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends BaseFrontController
{
    use SendsPasswordResetEmails;

    /**
     * @return mixed
     */
    public function broker()
    {
        return Password::broker('enzaime-users');
    }

    public function getIndex()
    {
        $this->setBodyClass('forgot-password-page');
        $this->setPageTitle(trans('enzaime-users::auth.forgot_password'));

        return $this->view(config('enzaime-auth.front_actions.forgot_password.view') ?: 'enzaime-users::front.auth.forgot-password');
    }

    public function postIndex(ForgotPasswordRequest $request)
    {
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response != Password::RESET_LINK_SENT) {
            flash_messages()
                ->addMessages(trans($response), 'error')
                ->showMessagesOnSession();
            return redirect()->back();
        }

        flash_messages()
            ->addMessages(trans($response), 'success')
            ->showMessagesOnSession();

        return redirect()->to(route('front.web.resolve-pages.get'));
    }
}
