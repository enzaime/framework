<?php namespace Enzaime\Base\Users\Http\DataTables;

use Carbon\Carbon;
use Enzaime\Base\Users\Models\User;
use Enzaime\Base\Users\Actions\DeleteUserAction;
use Enzaime\Base\Users\Actions\UpdateUserAction;
use Enzaime\Base\Users\Repositories\UserRepository;
use Enzaime\Base\Http\DataTables\AbstractDataTables;
use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;

class UsersListDataTable extends AbstractDataTables
{
    /**
     * @var User
     */
    protected $model;

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var array|\Illuminate\Http\Request|string
     */
    protected $request;

    public function __construct(UserRepositoryContract $repository)
    {
        $this->model = User::select('id', 'created_at', 'avatar', 'username', 'name', 'email', 'deleted_at')
            ->withTrashed();

        $this->request = request();

        $this->repository = $repository;
    }

    public function headings(): array
    {
        return [
            'avatar' => [
                'title' => trans('enzaime-users::datatables.heading.avatar'),
                'width' => '1%',
            ],
            'name' => [
                'title' => trans('enzaime-users::datatables.heading.name'),
                'width' => '15%',
            ],
            'username' => [
                'title' => trans('enzaime-users::datatables.heading.username'),
                'width' => '10%',
            ],
            'email' => [
                'title' => trans('enzaime-users::datatables.heading.email'),
                'width' => '15%',
            ],
            // 'status' => [
            //     'title' => trans('enzaime-users::datatables.heading.status'),
            //     'width' => '5%',
            // ],
            'created_at' => [
                'title' => trans('enzaime-users::datatables.heading.created_at'),
                'width' => '12%',
            ],
            'roles' => [
                'title' => trans('enzaime-users::datatables.heading.roles'),
                'width' => '12%',
            ],
            'actions' => [
                'title' => trans('enzaime-core::datatables.heading.actions'),
                'width' => '20%',
            ],
        ];
    }

    public function columns(): array
    {
        return [
            ['data' => 'id', 'name' => 'id', 'searchable' => false, 'orderable' => false],
            ['data' => 'avatar', 'name' => 'avatar', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'name'],
            ['data' => 'username', 'name' => 'username'],
            ['data' => 'email', 'name' => 'email'],
            // ['data' => 'status', 'name' => 'status'],
            ['data' => 'created_at', 'name' => 'created_at', 'searchable' => false],
            ['data' => 'roles', 'name' => 'roles', 'searchable' => false, 'orderable' => false],
            ['data' => 'actions', 'name' => 'actions', 'searchable' => false, 'orderable' => false],
        ];
    }

    /**
     * @return string
     */
    public function run(): string
    {
        $this->setAjaxUrl(route('admin::users.index.post'), 'POST');

        $this
            ->addFilter(2, form()->text('name', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ])) ->addFilter(3, form()->text('username', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ]))
            ->addFilter(4, form()->email('email', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ]));
            // ->addFilter(4, form()->select('status', [
            //     'without_trashed' => trans('enzaime-core::datatables.select') . '...',
            //     1 => trans('enzaime-core::base.status.activated'),
            //     0 => trans('enzaime-core::base.status.disabled'),
            //     'deleted' => trans('enzaime-core::base.status.deleted'),
            // ], '', ['class' => 'form-control form-filter input-sm']));

        $this->withGroupActions([
            '' => trans('enzaime-core::datatables.select') . '...',
            'deleted' => trans('enzaime-core::datatables.delete_these_items'),
            'activated' => trans('enzaime-core::datatables.active_these_items'),
            'disabled' => trans('enzaime-core::datatables.disable_these_items'),
        ]);

        return $this->view();
    }

    /**
     * @return mixed
     */
    protected function fetchDataForAjax()
    {
        return enzaime_datatable()->of($this->model)
            ->rawColumns(['actions', 'avatar'])
            ->filterColumn('status', function ($query, $keyword) {
                if ($keyword === 'deleted') {
                    return $query->whereNotNull('deleted_at');
                } else if ($keyword == 'without_trashed') {
                    return $query->whereNull('deleted_at');
                }

                return $query
                    ->whereNull('deleted_at')
                    ->where('status', '=', $keyword);
            })
            ->editColumn('avatar', function ($item) {
                return '<img src="' . get_image($item->avatar) . '" width="50" height="50">';
            })
            ->editColumn('id', function ($item) {
                return form()->customCheckbox([['id[]', $item->id]]);
            })
            ->editColumn('status', function ($item) {
                if ($item->trashed()) {
                    return html()->label(trans('enzaime-core::base.status.deleted'), 'deleted');
                }

                $status = $item->status ? 'activated' : 'disabled';
                return html()->label(trans('enzaime-core::base.status.' . $status), $status);
            })
            ->editColumn('created_at', function ($item) {
                if ($item->created_at) {
                    return Carbon::parse($item->created_at)->format('d M\'y');
                }
                return '';
              })
            ->addColumn('roles', function ($item) {
                $result = [];
                $roles = $this->repository->getRoles($item);
                if ($roles) {
                    foreach ($roles as $key => $row) {
                        $result[] = $row->name;
                    }
                }
                return implode(', ', $result);
            })
            ->addColumn('actions', function ($item) {
                /*Edit link*/
                $activeLink = route('admin::users.update-status.post', ['id' => $item->id, 'status' => 1]);
                $disableLink = route('admin::users.update-status.post', ['id' => $item->id, 'status' => 0]);
                $deleteLink = route('admin::users.delete.post', ['id' => $item->id]);
                $forceDelete = route('admin::users.force-delete.post', ['id' => $item->id]);
                $restoreLink = route('admin::users.restore.post', ['id' => $item->id]);

                /*Buttons*/
                $editBtn = link_to(route('admin::users.edit.get', ['id' => $item->id]), trans('enzaime-core::datatables.edit'), ['class' => 'btn btn-outline green btn-sm']);

                $activeBtn = ($item->status != 1 && !$item->trashed()) ? form()->button(trans('enzaime-core::datatables.active'), [
                    'title' => trans('enzaime-core::datatables.active_this_item'),
                    'data-ajax' => $activeLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline blue btn-sm ajax-link',
                ]) : '';
                $disableBtn = ($item->status != 0 && !$item->trashed()) ? form()->button(trans('enzaime-core::datatables.disable'), [
                    'title' => trans('enzaime-core::datatables.disable_this_item'),
                    'data-ajax' => $disableLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline yellow-lemon btn-sm ajax-link',
                ]) : '';

                $deleteBtn = (!$item->trashed())
                    ? form()->button(trans('enzaime-core::datatables.delete'), [
                        'title' => trans('enzaime-core::datatables.delete_this_item'),
                        'data-ajax' => $deleteLink,
                        'data-method' => 'POST',
                        'data-toggle' => 'confirmation',
                        'class' => 'btn btn-outline red-sunglo btn-sm ajax-link',
                    ])
                    : form()->button(trans('enzaime-core::datatables.force_delete'), [
                        'title' => trans('enzaime-core::datatables.force_delete_this_item'),
                        'data-ajax' => $forceDelete,
                        'data-method' => 'POST',
                        'data-toggle' => 'confirmation',
                        'class' => 'btn btn-outline red-sunglo btn-sm ajax-link',
                    ]) . form()->button(trans('enzaime-core::datatables.restore'), [
                        'title' => trans('enzaime-core::datatables.restore_this_item'),
                        'data-ajax' => $restoreLink,
                        'data-method' => 'POST',
                        'data-toggle' => 'confirmation',
                        'class' => 'btn btn-outline blue btn-sm ajax-link',
                    ]);

                $activeBtn = ($item->status != 1) ? $activeBtn : '';
                $disableBtn = ($item->status != 0) ? $disableBtn : '';

                return $editBtn . $activeBtn . $disableBtn . $deleteBtn;
            });
    }

    /**
     * Handle group actions
     * @return array
     */
    protected function groupAction(): array
    {
        $data = [];
        if ($this->request->input('customActionType', null) == 'group_action') {
            $actionValue = $this->request->input('customActionValue', 'activated');

            if (!has_permissions(get_current_logged_user(), ['edit-other-users'])) {
                return [
                    'customActionMessage' => trans('enzaime-acl::base.do_not_have_permission'),
                    'customActionStatus' => 'danger',
                ];
            }

            $ids = collect($this->request->input('id', []))->filter(function ($value, $index) {
                return (int)$value !== (int)get_current_logged_user_id();
            })->toArray();

            switch ($actionValue) {
                case 'deleted':
                    if (!has_permissions(get_current_logged_user(), ['delete-users'])) {
                        $data['customActionMessage'] = trans('enzaime-acl::base.do_not_have_permission');
                        $data['customActionStatus'] = 'danger';
                        return $data;
                    }

                    $action = app(DeleteUserAction::class);
                    foreach ($ids as $id) {
                        $action->run($id);
                    }
                    break;
                default:
                    $action = app(UpdateUserAction::class);

                    foreach ($ids as $id) {
                        $action->run($id, [
                            'status' => $actionValue,
                        ]);
                    }
                    break;
            }

            $data['customActionMessage'] = trans('enzaime-core::base.form.request_completed');
            $data['customActionStatus'] = 'success';
        }
        return $data;
    }
}
