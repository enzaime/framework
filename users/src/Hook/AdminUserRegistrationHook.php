<?php namespace Enzaime\Base\Users\Hook;


class AdminUserRegistrationHook
{
    /**
     * @var UserRepository
     */
   

    public function handle($user, $subdomainId = null)
    {
        return set_role_to_users(SUPER_ADMIN_ROLE, [$user->id], $subdomainId);
    }
}
