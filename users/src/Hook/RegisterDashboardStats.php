<?php namespace Enzaime\Base\Users\Hook;

use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;
use Enzaime\Base\Users\Repositories\UserRepository;

class RegisterDashboardStats
{
    /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function handle()
    {
        echo view('enzaime-users::admin.dashboard-stats.stat-box', [
            'count' => $this->repository->count(),
        ]);
    }
}
