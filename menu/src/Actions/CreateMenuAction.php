<?php namespace Enzaime\Base\Menu\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\Menu\Repositories\Contracts\MenuRepositoryContract;
use Enzaime\Base\Menu\Repositories\MenuRepository;

class CreateMenuAction extends AbstractAction
{
    /**
     * @var MenuRepository
     */
    protected $repository;

    public function __construct(MenuRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param array|null $menuStructure
     * @return array
     */
    public function run(array $data, array $menuStructure = null)
    {
        do_action(BASE_ACTION_BEFORE_CREATE, ENZAIME_MENUS, 'create.post');

        $data['created_by'] = get_current_logged_user_id();

        $result = $this->repository->createMenu($data, $menuStructure);

        do_action(BASE_ACTION_AFTER_CREATE, ENZAIME_MENUS, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
