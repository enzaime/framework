<?php namespace Enzaime\Base\Menu\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\Menu\Repositories\Contracts\MenuRepositoryContract;
use Enzaime\Base\Menu\Repositories\MenuRepository;

class DeleteMenuAction extends AbstractAction
{
    /**
     * @var MenuRepository
     */
    protected $repository;

    public function __construct(MenuRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return array
     */
    public function run($id)
    {
        $id = do_filter(BASE_FILTER_BEFORE_DELETE, $id, ENZAIME_MENUS);

        $result = $this->repository->delete($id);

        do_action(BASE_ACTION_AFTER_DELETE, ENZAIME_MENUS, $id, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
