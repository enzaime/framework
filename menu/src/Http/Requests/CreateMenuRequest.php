<?php namespace Enzaime\Base\Menu\Http\Requests;

use Enzaime\Base\Http\Requests\Request;

class CreateMenuRequest extends Request
{
    public function rules()
    {
        return [
            'title' => 'string|max:255|required',
            'slug' => 'string|max:255|nullable||unique:' . enzaime_db_prefix() . 'menus,slug',
            'status' => 'required',
            'menu_structure' => 'required',
            'deleted_nodes' => 'required'
        ];
    }
}
