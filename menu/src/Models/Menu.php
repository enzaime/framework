<?php namespace Enzaime\Base\Menu\Models;

use Enzaime\Base\Menu\Models\Contracts\MenuModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class Menu extends BaseModel implements MenuModelContract
{
    protected $table = 'menus';

    protected $primaryKey = 'id';

    protected $subdomain = true;

    protected $fillable = [
        'title', 'slug', 'status', 'created_by', 'updated_by', 'subdomain_id'
    ];

    public $timestamps = true;
}
