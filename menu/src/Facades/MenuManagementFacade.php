<?php namespace Enzaime\Base\Menu\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Menu\Support\MenuManagement;

class MenuManagementFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return MenuManagement::class;
    }
}
