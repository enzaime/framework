<?php namespace Enzaime\Base\Menu\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Menu\Models\Menu;
use Enzaime\Base\Menu\Models\MenuNode;
use Enzaime\Base\Menu\Repositories\Contracts\MenuNodeRepositoryContract;
use Enzaime\Base\Menu\Repositories\Contracts\MenuRepositoryContract;
use Enzaime\Base\Menu\Repositories\MenuNodeRepository;
use Enzaime\Base\Menu\Repositories\MenuNodeRepositoryCacheDecorator;
use Enzaime\Base\Menu\Repositories\MenuRepository;
use Enzaime\Base\Menu\Repositories\MenuRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MenuRepositoryContract::class, function () {
            $repository = new MenuRepository(new Menu());

            if (config('enzaime-caching.repository.enabled')) {
                return new MenuRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
        $this->app->bind(MenuNodeRepositoryContract::class, function () {
            $repository = new MenuNodeRepository(new MenuNode());

            if (config('enzaime-caching.repository.enabled')) {
                return new MenuNodeRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
