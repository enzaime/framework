<?php

if (!defined('ENZAIME_THEMES_MANAGEMENT')) {
    define('ENZAIME_THEMES_MANAGEMENT', 'enzaime-themes-management');
}

if (!defined('ENZAIME_THEME_OPTIONS')) {
    define('ENZAIME_THEME_OPTIONS', 'enzaime-theme-options');
}

if (!defined('ENZAIME_THEME_BEFORE_INSTALL')) {
    define('ENZAIME_THEME_BEFORE_INSTALL', 'enzaime-themes-management.theme-before-install');
}

if (!defined('ENZAIME_THEME_AFTER_INSTALL')) {
    define('ENZAIME_THEME_AFTER_INSTALL', 'enzaime-themes-management.theme-after-install');
}

if (!defined('ENZAIME_THEME_BEFORE_UPDATE')) {
    define('ENZAIME_THEME_BEFORE_UPDATE', 'enzaime-themes-management.theme-before-update');
}

if (!defined('ENZAIME_THEME_UPDATED')) {
    define('ENZAIME_THEME_UPDATED', 'enzaime-themes-management.theme-updated');
}

if (!defined('ENZAIME_THEME_BEFORE_ENABLE')) {
    define('ENZAIME_THEME_BEFORE_ENABLE', 'enzaime-themes-management.theme-before-enable');
}

if (!defined('ENZAIME_THEME_ENABLED')) {
    define('ENZAIME_THEME_ENABLED', 'enzaime-themes-management.theme-enabled');
}

if (!defined('ENZAIME_THEME_BEFORE_DISABLE')) {
    define('ENZAIME_THEME_BEFORE_DISABLE', 'enzaime-themes-management.theme-before-disable');
}

if (!defined('ENZAIME_THEME_DISABLED')) {
    define('ENZAIME_THEME_DISABLED', 'enzaime-themes-management.theme-disabled');
}

if (!defined('ENZAIME_SUBDOMAIN_THEME_BEFORE_ENABLE')) {
    define('ENZAIME_SUBDOMAIN_THEME_BEFORE_ENABLE', 'enzaime-themes-management.subdomain-theme-before-enable');
}

if (!defined('ENZAIME_SUBDOMAIN_THEME_ENABLED')) {
    define('ENZAIME_SUBDOMAIN_THEME_ENABLED', 'enzaime-themes-management.subdomain-theme-enabled');
}

if (!defined('ENZAIME_SUBDOMAIN_THEME_BEFORE_DISABLE')) {
    define('ENZAIME_SUBDOMAIN_THEME_BEFORE_DISABLE', 'enzaime-themes-management.subdomain-theme-before-disable');
}

if (!defined('ENZAIME_SUBDOMAIN_THEME_DISABLED')) {
    define('ENZAIME_SUBDOMAIN_THEME_DISABLED', 'enzaime-themes-management.subdomain-theme-disabled');
}

if (!defined('ENZAIME_THEME_BEFORE_UNINSTALL')) {
    define('ENZAIME_THEME_BEFORE_UNINSTALL', 'enzaime-themes-management.theme-before-uninstall');
}

if (!defined('ENZAIME_THEME_AFTER_UNINSTALL')) {
    define('ENZAIME_THEME_AFTER_UNINSTALL', 'enzaime-themes-management.theme-after-uninstall');
}
