<?php namespace Enzaime\Base\ThemesManagement\Models;

use Enzaime\Base\ThemesManagement\Models\Contracts\ThemeOptionModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class ThemeOption extends BaseModel implements ThemeOptionModelContract
{
    protected $table = 'theme_options';

    protected $primaryKey = 'id';
    
    protected $subdomain = false;
    
    protected $fillable = [
        'theme_id',
        'key',
        'value',
        'subdomain_id',
        
    ];

    public $timestamps = false;

    public function theme()
    {
        return $this->belongsTo(Theme::class, 'theme_id');
    }
}
