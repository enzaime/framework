<?php namespace Enzaime\Base\ThemesManagement\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;
use Enzaime\Base\ThemesManagement\Repositories\Contracts\ThemeOptionRepositoryContract;

class ThemeOptionRepository extends EloquentBaseRepository implements ThemeOptionRepositoryContract
{
    /**
     * @var mixed|null
     */
    protected $currentTheme;

    /**
     * @param $id
     * @return array
     */
    public function getOptionsByThemeId($id)
    {
        $query = $this->model
            ->join(enzaime_db_prefix() . 'themes', enzaime_db_prefix() . 'theme_options.theme_id', '=', enzaime_db_prefix() . 'themes.id')
            ->where(enzaime_db_prefix() . 'themes.id', '=', $id)
            ->select(enzaime_db_prefix() . 'theme_options.key', enzaime_db_prefix() . 'theme_options.value')
            ->get();
        return $query->pluck('value', 'key')->toArray();
    }

    /**
     * @param $alias
     * @return array
     */
    public function getOptionsByThemeAlias($alias)
    {
        $query = $this->model
            ->join(enzaime_db_prefix() . 'themes', enzaime_db_prefix() . 'theme_options.theme_id', '=', enzaime_db_prefix() . 'themes.id')
            ->where(enzaime_db_prefix() . 'themes.alias', '=', $alias)
            ->select(enzaime_db_prefix() . 'theme_options.key', enzaime_db_prefix() . 'theme_options.value')
            ->get();
        return $query->pluck('value', 'key')->toArray();
    }

    /**
     * @param array $options
     * @return bool
     */
    public function updateOptions($options = [], $subdomainId = null)
    {
        foreach ($options as $key => $row) {
            $result = $this->updateOption($key, $row, $subdomainId);
            if (!$result) {
                return $result;
            }
        }
        return true;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function updateOption($key, $value, $subdomainId = null)
    {
        if (!$this->currentTheme) {
            $this->currentTheme = cms_theme_options()->getCurrentTheme();
        }

        if (!$this->currentTheme) {
            return false;
        }

        $option = $this->model
            ->where([
                'key' => $key,
                'theme_id' => array_get($this->currentTheme, 'id'),
                'subdomain_id' => $subdomainId
            ])
            ->select(['id', 'key', 'value'])
            ->first();

        $result = $this->createOrUpdate($option, [
            'key' => $key,
            'value' => $value,
            'theme_id' => array_get($this->currentTheme, 'id'),
            'subdomain_id' => $subdomainId
        ]);

        if (!$result) {
            return false;
        }

        return true;
    }
}
