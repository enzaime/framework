<?php namespace Enzaime\Base\ThemesManagement\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;
use Enzaime\Base\ThemesManagement\Repositories\Contracts\ThemeRepositoryContract;

class ThemeRepository extends EloquentBaseRepository implements ThemeRepositoryContract
{
    /**
     * @param $alias
     * @return mixed
     */
    public function getByAlias($alias)
    {
        return $this->model->where('alias', '=', $alias)->first();
    }
    /**
     * @param $alias|$id
     * @return mixed
     */
    public function getOptions($aliasOrId)
    {
        
        $theme = $this->model->where('alias', '=', $aliasOrId)
            ->orWhere('id', $aliasOrId)->first();
        
        return $theme->themeOptions->pluck('value', 'key')->toArray();    
    }


}
