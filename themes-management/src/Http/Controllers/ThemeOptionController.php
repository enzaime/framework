<?php namespace Enzaime\Base\ThemesManagement\Http\Controllers;

use Enzaime\Base\Http\Controllers\BaseAdminController;
use Enzaime\Base\ThemesManagement\Repositories\Contracts\ThemeOptionRepositoryContract;
use Enzaime\Base\ThemesManagement\Repositories\ThemeOptionRepository;

class ThemeOptionController extends BaseAdminController
{
    protected $module = 'enzaime-themes-management';

    /**
     * @param ThemeOptionRepository $repository
     */
    public function __construct(ThemeOptionRepositoryContract $repository)
    {
        parent::__construct();

        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->getDashboardMenu('enzaime-theme-options');
            $this->breadcrumbs->addLink(trans($this->module . '::base.theme_options'));

            return $next($request);
        });
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $this->setPageTitle(trans($this->module . '::base.theme_options'));

        return do_filter(BASE_FILTER_CONTROLLER, $this, ENZAIME_THEME_OPTIONS, 'index.get')->viewAdmin('theme-options-index');
    }

    public function postIndex()
    {
        $data = $this->request->except([
            '_token',
            '_tab',
        ]);

        /**
         * Filter
         */
        $data = do_filter('theme-options.before-edit.post', $data, $this);

        $result = $this->repository->updateOptions($data, get_subdomain_id());

        $msgType = !$result ? 'danger' : 'success';
        $msg = $result ? trans('enzaime-core::base.form.request_completed') : trans('enzaime-core::base.form.error_occurred');

        flash_messages()
            ->addMessages($msg, $msgType)
            ->showMessagesOnSession();

        do_action(BASE_ACTION_AFTER_UPDATE, ENZAIME_THEME_OPTIONS, $data, $result);

        return redirect()->back();
    }
}
