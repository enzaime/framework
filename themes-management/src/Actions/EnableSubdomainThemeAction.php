<?php namespace Enzaime\Base\ThemesManagement\Actions;

use Enzaime\Base\Actions\AbstractAction;

class EnableSubdomainThemeAction extends AbstractAction
{
    /**
     * @param $alias
     * @return array
     */
    public function run($alias)
    {
        do_action(ENZAIME_SUBDOMAIN_THEME_BEFORE_ENABLE, $alias);

        $theme = get_theme_information($alias);

        if (!$theme) {
            return $this->error('Plugin not exists');
        }

        $checkRelatedModules = check_module_require($theme);
        
        if ($checkRelatedModules['error']) {
            $messages = [];
            foreach ($checkRelatedModules['messages'] as $message) {
                $messages[] = $message;
            }
            return $this->error($messages);
        }

        themes_management()->enableSubdomainTheme($alias);

        do_action(ENZAIME_SUBDOMAIN_THEME_ENABLED, $alias);


        return $this->success('Your theme has been enabled');
    }
}
