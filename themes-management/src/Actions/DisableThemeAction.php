<?php namespace Enzaime\Base\ThemesManagement\Actions;

use Enzaime\Base\Actions\AbstractAction;

class DisableThemeAction extends AbstractAction
{
    /**
     * @param $alias
     * @return array
     */
    public function run($alias)
    {
        do_action(ENZAIME_THEME_BEFORE_DISABLE, $alias);

        themes_management()->disableTheme($alias);

        do_action(ENZAIME_THEME_DISABLED, $alias);

        modules_management()->refreshComposerAutoload();

        return $this->success('Your theme has been disabled');
    }
}
