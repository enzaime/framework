<?php namespace Enzaime\Base\ThemesManagement\Actions;

use Enzaime\Base\Actions\AbstractAction;

class DisableSubdomainThemeAction extends AbstractAction
{
    /**
     * @param $alias
     * @return array
     */
    public function run($alias)
    {
        do_action(ENZAIME_SUBDOMAIN_THEME_BEFORE_DISABLE, $alias);

        themes_management()->disableSubdomainTheme($alias);

        do_action(ENZAIME_SUBDOMAIN_THEME_DISABLED, $alias);

        modules_management()->refreshComposerAutoload();

        return $this->success('Your theme has been disabled');
    }
}
