<?php namespace Enzaime\Base\ThemesManagement\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->generatorCommands();
        $this->otherCommands();
    }

    protected function generatorCommands()
    {
        $this->commands([
            \Enzaime\Base\ThemesManagement\Console\Generators\MakeTheme::class,
        ]);
    }

    protected function otherCommands()
    {
        $this->commands([
            \Enzaime\Base\ThemesManagement\Console\Commands\EnableThemeCommand::class,
            \Enzaime\Base\ThemesManagement\Console\Commands\DisableThemeCommand::class,
            \Enzaime\Base\ThemesManagement\Console\Commands\InstallThemeCommand::class,
            \Enzaime\Base\ThemesManagement\Console\Commands\UpdateThemeCommand::class,
            \Enzaime\Base\ThemesManagement\Console\Commands\UninstallThemeCommand::class,
            \Enzaime\Base\ThemesManagement\Console\Commands\GetAllThemesCommand::class,
        ]);
    }
}
