<?php namespace Enzaime\Base\ThemesManagement\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\ThemesManagement\Support\ThemesManagement;

class ThemesManagementFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ThemesManagement::class;
    }
}
