<?php namespace Enzaime\Base\Hook\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Hook\Support\Filters;

class FiltersFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Filters::class;
    }
}
