<?php namespace Enzaime\Base\Hook\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Hook\Support\Actions;

class ActionsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Actions::class;
    }
}
