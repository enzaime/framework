<?php
use Enzaime\Base\ACL\Repositories\Contracts\RoleRepositoryContract;
use Enzaime\Base\ACL\Repositories\Contracts\PermissionRepositoryContract;

if (!function_exists('check_user_acl')) {
    /**
     * @return \Enzaime\Base\ACL\Support\CheckUserACL
     */
    function check_user_acl()
    {
        return \Enzaime\Base\ACL\Facades\CheckUserACLFacade::getFacadeRoot();
    }
}

if (!function_exists('acl_permission')) {
    /**
     * Get the PermissionRepository instance.
     *
     * @return \Enzaime\Base\ACL\Repositories\PermissionRepository
     */
    function acl_permission()
    {
        return app(PermissionRepositoryContract::class);
    }
}

if (!function_exists('has_permissions')) {
    /**
     * @param \Enzaime\Base\Users\Models\User $user
     * @param array $permissions
     * @return bool
     */
    function has_permissions($user, array $permissions = [])
    {
        if (!$user) {
            return false;
        }

        if (!$permissions) {
            return true;
        }

        /**
         * @var \Enzaime\Base\Users\Repositories\UserRepository $userRepo
         */
        $userRepo = app(\Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract::class);
        return $userRepo->hasPermission($user, $permissions);
    }
}

if (!function_exists('has_roles')) {
    /**
     * @param \Enzaime\Base\Users\Models\User $user
     * @param array $roles
     * @return bool
     */
    function has_roles($user, array $roles = [])
    {
        if (!$user) {
            return false;
        }

        if (!$roles) {
            return true;
        }

        /**
         * @var \Enzaime\Base\Users\Repositories\UserRepository $userRepo
         */
        $userRepo = app(\Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract::class);
        return $userRepo->hasRole($user, $roles);
    }
}



if (!function_exists('set_role_to_users')) {
    /**
     * @param  string|int $role
     * @param array $userIds
     * @param int $subdomainId
     * @return bool
     */
    function set_role_to_users($role, array $userIds = [], $subdomainId = null)
    {

        $roleRepo = app(RoleRepositoryContract::class);

        return $roleRepo->syncUsers($role, $userIds, $subdomainId);
    }
}