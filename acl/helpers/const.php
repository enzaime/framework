<?php

if (!defined('ENZAIME_ACL')) {
    define('ENZAIME_ACL', 'enzaime-acl');
}

if (!defined('ENZAIME_ACL_PERMISSION')) {
    define('ENZAIME_ACL_PERMISSION', 'enzaime-acl.permission');
}

if (!defined('ENZAIME_ACL_ROLE')) {
    define('ENZAIME_ACL_ROLE', 'enzaime-acl.role');
}

if (!defined('SUPER_ADMIN_ROLE')) {
    define('SUPER_ADMIN_ROLE', 'super-admin');
}
