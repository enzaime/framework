<?php namespace Enzaime\Base\ACL\Http\DataTables;

use Enzaime\Base\ACL\Actions\DeleteRoleAction;
use Enzaime\Base\ACL\Models\Role;
use Enzaime\Base\Http\DataTables\AbstractDataTables;

class RolesListDataTable extends AbstractDataTables
{
    /**
     * @var Role
     */
    protected $model;

    /**
     * @var string
     */
    protected $screenName = ENZAIME_ACL_ROLE;

    public function __construct()
    {
        $this->model = do_filter(
            FRONT_FILTER_DATA_TABLES_MODEL,
            Role::select('id', 'name', 'slug')->bySubdomain(),
            $this->screenName
        );
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'name' => [
                'title' => trans('enzaime-acl::datatables.role.heading.name'),
                'width' => '50%',
            ],
            'slug' => [
                'title' => trans('enzaime-acl::datatables.role.heading.slug'),
                'width' => '30%',
            ],
            'actions' => [
                'title' => trans('enzaime-core::datatables.heading.actions'),
                'width' => '20%',
            ],
        ];
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            ['data' => 'id', 'name' => 'id', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'name'],
            ['data' => 'slug', 'name' => 'slug'],
            ['data' => 'actions', 'name' => 'actions', 'searchable' => false, 'orderable' => false],
        ];
    }

    /**
     * @return string
     */
    public function run(): string
    {
        $this->setAjaxUrl(route('admin::acl-roles.index.post'), 'POST');

        $this
            ->addFilter(1, form()->text('name', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ]))
            ->addFilter(2, form()->text('slug', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ]));

        $this->withGroupActions([
            '' => trans('enzaime-core::datatables.select') . '...',
            'deleted' => trans('enzaime-core::datatables.delete_these_items'),
        ]);

        return $this->view();
    }

    /**
     * @return mixed
     */
    protected function fetchDataForAjax()
    {
        return enzaime_datatable()->of($this->model)
            ->rawColumns(['actions'])
            ->editColumn('id', function ($item) {
                return form()->customCheckbox([
                    ['id[]', $item->id]
                ]);
            })
            ->addColumn('actions', function ($item) {
                /*Edit link*/
                $deleteLink = route('admin::acl-roles.delete.post', ['id' => $item->id]);
                $editLink = route('admin::acl-roles.edit.get', ['id' => $item->id]);

                /*Buttons*/
                $editBtn = link_to($editLink, trans('enzaime-core::datatables.edit'), ['class' => 'btn btn-outline green btn-sm']);
                $deleteBtn = form()->button(trans('enzaime-core::datatables.delete'), [
                    'title' => trans('enzaime-core::datatables.delete_this_item'),
                    'data-ajax' => $deleteLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline red-sunglo btn-sm ajax-link',
                ]);

                return $editBtn . $deleteBtn;
            });
    }

    /**
     * @return array
     */
    protected function groupAction(): array
    {
        $request = request();

        $data = [];
        if ($request->input('customActionType', null) == 'group_action') {
            if(!has_permissions(get_current_logged_user(), ['delete-roles'])) {
                return [
                    'customActionMessage' => trans(ENZAIME_ACL . '::base.do_not_have_permission'),
                    'customActionStatus' => 'danger',
                ];
            }

            $ids = (array)$request->input('id', []);

            $action = app(DeleteRoleAction::class);
            foreach ($ids as $id) {
                $action->run($id);
            }

            $data['customActionMessage'] = trans(ENZAIME_ACL . '::base.delete_role_success');
            $data['customActionStatus'] = 'success';
        }
        return $data;
    }
}
