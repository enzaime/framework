<?php namespace Enzaime\Base\ACL\Repositories;

use Enzaime\Base\ACL\Models\Role;
use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;
use Enzaime\Base\ACL\Repositories\Contracts\RoleRepositoryContract;

class RoleRepository extends EloquentBaseRepository implements RoleRepositoryContract
{
    /**
     * The roles with these alias cannot be deleted
     * @var array
     */
    protected $cannotDelete = [];

    public function __construct(BaseModelContract $model)
    {
        parent::__construct($model);

        $this->cannotDelete = array_merge(config('enzaime-acl.cannot_delete_roles', []), ['super-admin']);
    }

    /**
     * @param $roleId
     * @param array $data
     * @return bool
     */
    public function syncPermissions($roleId, array $data)
    {
        try {
            $this->model
                ->find($roleId)
                ->permissions()
                ->sync($data);
            $this->resetModel();
        } catch (\Exception $exception) {
            $this->resetModel();
            return false;
        }

        return true;
    }

    /**
     * @param array|int $id
     * @return bool
     */
    public function deleteRole($id)
    {
        try {
            $this->model
                ->whereNotIn('slug', $this->cannotDelete)
                ->whereIn('id', (array)$id)
                ->delete();
            $this->resetModel();
        } catch (\Exception $exception) {
            $this->resetModel();
            return false;
        }
        return true;
    }

    /**
     * @param array $data
     * @return int
     */
    public function createRole(array $data, array $permissions = [])
    {
        $roleId = $this->create($data);

        /**
         * Sync permissions
         */
        if ($permissions) {
            $this->syncPermissions($roleId, $permissions);
        }

        return $roleId;
    }

    /**
     * @param $id
     * @param array $data
     * @param array $permissions
     * @return int|null
     */
    public function updateRole($id, array $data, array $permissions = [])
    {
        $roleId = $this->update($id, $data);

        /**
         * Sync permissions
         */
        if ($roleId) {
            $this->syncPermissions($roleId, $permissions);
        }

        return $roleId;
    }

    /**
     * @param Role|int $id
     * @return array
     */
    public function getRelatedPermissions($id)
    {
        if ($id instanceof Role) {
            $item = $id;
        } else {
            $item = $this->find($id);
        }

        if (!$item) {
            return [];
        }

        return $item->permissions()->allRelatedIds()->toArray();
    }

    /**
     * @param array $columns
     * @return Collection
     */
    public function get(array $columns = ['*'])
    {
        $this->applyCriteria();

        $result = $this->model->bySubdomain()->get($columns);

        $this->resetModel();

        return $result;
    }

    /**
     * @param Role|int $id
     * @return array
     */
    public function syncUsers($id, array $users, $subdomainId = null)
    {
      
        if ($id instanceof Role) {
            $item = $id;
        } else {
            $item = $this->find($id) ?:  $this->findWhere(['slug'=> $id]);
        }

        if (!$item || !count($users)) {
            return [];
        }

        $pivotData = [
            'created_by' => get_current_logged_user_id(),
            'subdomain_id' =>  $subdomainId ?: get_subdomain_id()
        ];

        $data = [];

        array_map(function($item) use ($pivotData, &$data) {
                if (is_array($item)) {
                    $data[$key] = array_merge($pivotData, $item);
                } else {
                    $data[$item] = $pivotData;
                }

            }, $users);

        return $item->users()->syncWithoutDetaching($data);
    }
}
