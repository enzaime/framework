<?php namespace Enzaime\Base\ACL\Actions;

use Enzaime\Base\ACL\Repositories\Contracts\RoleRepositoryContract;
use Enzaime\Base\ACL\Repositories\RoleRepository;
use Enzaime\Base\Actions\AbstractAction;

class CreateRoleAction extends AbstractAction
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    public function __construct(RoleRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param array $permissions
     * @return array
     */
    public function run(array $data, array $permissions = [])
    {
        do_action(BASE_ACTION_BEFORE_CREATE, ENZAIME_ACL_ROLE, 'create.post');
        
        $data['created_by'] = get_current_logged_user_id();

        $result = $this->repository->createRole($data, $permissions);

        do_action(BASE_ACTION_AFTER_CREATE, ENZAIME_ACL_ROLE, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
