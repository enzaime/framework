<?php namespace Enzaime\Base\ACL\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\ACL\Support\CheckCurrentUserACL;
use Enzaime\Base\ACL\Support\CheckUserACL;

class CheckUserACLFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CheckUserACL::class;
    }
}
