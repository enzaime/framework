<?php namespace Enzaime\Base\ACL\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\ACL\Models\Permission;
use Enzaime\Base\ACL\Models\Role;
use Enzaime\Base\ACL\Repositories\Contracts\PermissionRepositoryContract;
use Enzaime\Base\ACL\Repositories\Contracts\RoleRepositoryContract;
use Enzaime\Base\ACL\Repositories\PermissionRepository;
use Enzaime\Base\ACL\Repositories\PermissionRepositoryCacheDecorator;
use Enzaime\Base\ACL\Repositories\RoleRepository;
use Enzaime\Base\ACL\Repositories\RoleRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RoleRepositoryContract::class, function () {
            $repository = new RoleRepository(new Role);

            if (config('enzaime-caching.repository.enabled')) {
                return new RoleRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
        $this->app->bind(PermissionRepositoryContract::class, function () {
            $repository = new PermissionRepository(new Permission);

            if (config('enzaime-caching.repository.enabled')) {
                return new PermissionRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
