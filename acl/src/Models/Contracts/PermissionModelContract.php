<?php namespace Enzaime\Base\ACL\Models\Contracts;

interface PermissionModelContract
{
    /**
     * @return mixed
     */
    public function roles();
}
