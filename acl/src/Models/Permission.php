<?php namespace Enzaime\Base\ACL\Models;

use Enzaime\Base\ACL\Models\Contracts\PermissionModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class Permission extends BaseModel implements PermissionModelContract
{
    protected $table = 'permissions';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'slug', 'module', 'group', 'description'];

    public $timestamps = false;
    public $subdomain = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, enzaime_db_prefix() . 'roles_permissions', 'permission_id', 'role_id');
    }
}
