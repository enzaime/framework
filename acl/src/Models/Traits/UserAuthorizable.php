<?php namespace Enzaime\Base\ACL\Models\Traits;

use Enzaime\Base\ACL\Models\Role;

trait UserAuthorizable
{
    /**
     * Set relationship
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, enzaime_db_prefix() . 'users_roles', 'user_id', 'role_id')
            ->wherePivot('subdomain_id', get_subdomain_id())
            ->withTimestamps();
    }

    /**
     * Get all roles and permissions of current user
     */
    public function setupUser()
    {
        if (!$this->id || check_user_acl()->getRoles($this->id)) {
            return;
        }

        $relatedRoles = $this->roles;
       
        check_user_acl()->pushRoles($this->id, $relatedRoles->pluck('slug')->toArray());
        $relatedPermissions = static::join(enzaime_db_prefix() . 'users_roles', enzaime_db_prefix() . 'users_roles.user_id', '=', enzaime_db_prefix() . 'users.id')
            ->join(enzaime_db_prefix() . 'roles', enzaime_db_prefix() . 'users_roles.role_id', '=', enzaime_db_prefix() . 'roles.id')
            ->join(enzaime_db_prefix() . 'roles_permissions', enzaime_db_prefix() . 'roles_permissions.role_id', '=', enzaime_db_prefix() . 'roles.id')
            ->join(enzaime_db_prefix() . 'permissions', enzaime_db_prefix() . 'roles_permissions.permission_id', '=', enzaime_db_prefix() . 'permissions.id')
            ->where(enzaime_db_prefix() . 'users.id', '=', $this->id)
            ->distinct()
            ->groupBy(enzaime_db_prefix() . 'permissions.id', enzaime_db_prefix() . 'permissions.slug')
            ->select(enzaime_db_prefix() . 'permissions.slug', enzaime_db_prefix() . 'permissions.id')
            ->get()
            ->pluck('slug')
            ->toArray();
        check_user_acl()->pushPermissions($this->id, $relatedPermissions);
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        $this->setupUser();
        
       

        if (check_user_acl()->hasRoles($this->id, ['super-admin']) 
            || check_user_acl()->hasRoles($this->id, ['system-admin'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array|string $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        if (!is_array($roles)) {
            $roles = func_get_args();
        }

        if (!$roles) {
            return true;
        }

        $roles = array_values($roles);

        if (check_user_acl()->hasRoles($this->id, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * @param string|array $permissions
     * @return bool
     */
    public function hasPermission($permissions)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }


        if (!is_array($permissions)) {
            $permissions = func_get_args();
        }

        if (!$permissions) {
            return true;
        }

        $permissions = array_values($permissions);
        if (check_user_acl()->hasPermissions($this->id, $permissions)) {
            return true;
        }

        return false;
    }
}
