<?php namespace Enzaime\Base\ACL\Models;

use Enzaime\Base\ACL\Models\Contracts\RoleModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;
use Enzaime\Base\Users\Models\User;

class Role extends BaseModel implements RoleModelContract
{
    protected $table = 'roles';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'slug', 'created_by', 'updated_by'];

    public $timestamps = true;
    protected $subdomain = true;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, enzaime_db_prefix() . 'roles_permissions', 'role_id', 'permission_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, enzaime_db_prefix() . 'users_roles', 'role_id', 'user_id');
    }

    /**
     * Setter
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function scopeBySubdomain($query, $subdomainId = null)
    {
        $subdomainId = $subdomainId ?: get_subdomain_id();        

        return $query->where(function($query) use ($subdomainId){
                return $query->where('subdomain_id', $subdomainId)
                    ->orWhereNull('subdomain_id');
            })->where('id', '>', 1);
    }
}
