<?php

use Enzaime\Subdomain\Facades\SubdomainFacade;


if (!function_exists('subdomain')) {
    /**
     * @return Model|null
     */
    function subdomain()
    {
       return SubdomainFacade::getSubdomain();
    }
}


if (!function_exists('get_subdomain_name')) {
    /**
     * @return string|null
     */
    function get_subdomain_name()
    {
       return SubdomainFacade::getSubdomainName();
    }
}

if (!function_exists('get_subdomain_id')) {
    /**
     * @return int|null
     */
    function get_subdomain_id()
    {
       return subdomain() ? subdomain()->id : null;
    }
}

if (!function_exists('get_subdomain_plugins')) {
    /**
     * @return Collection|array
     */
    function get_subdomain_plugins($alias = null)
    {
        return enzaime_plugins()->getPluginBySubdomain($alias);
    }
}


if (!function_exists('get_current_subdomain_theme')) {
    /**
     * @return Collection|array
     */
    function get_current_subdomain_theme()
    {
        return SubdomainFacade::getCurrentTheme();
    }
}