<?php namespace Enzaime\Subdomain\Providers;

use Enzaime\Base\Users\Models\User;
use Illuminate\Support\ServiceProvider;
use Enzaime\Subdomain\Repositories\SubdomainRepository;
use Enzaime\Subdomain\Repositories\Contracts\SubdomainRepositoryContract;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubdomainRepositoryContract::class, function () {
            return new SubdomainRepository(new User());
        });
    }
}
