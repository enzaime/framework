<?php namespace Enzaime\Subdomain\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SubdomainScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if ($model->hasSubdomain()) {
            $builder->where(function($query) use ($model) {
                $key = "{$model->getTable()}.{$model->getSubdomainKey()}";
                return $query->where($key , get_subdomain_id())
                    ->orWhereNull($key);
            });
        }
    }
}