<?php namespace Enzaime\Subdomain\Repositories\Contracts;

use Enzaime\Base\Models\Contracts\BaseModelContract;

interface SubdomainRepositoryContract
{
    /**
     * @param array $data
     * @return int
     */
    public function createSubdomain(array $data);

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function createOrUpdateSubdomain($id, array $data);

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function updateSubdomain($id, array $data);

    /**
     * @param int|BaseModelContract|array $id
     * @param bool $force
     * @return bool
     */
    public function deleteSubdomain($id, $force = false);


    /**
     * @param string $name
     * @return bool
     */
    public function findByName($name);


    /**
     * @param User|int $subdomain
     * @param boolean $onlyEnabled
     * @return array
     */
    public function getPlugins($subdomain);
    
    /**
     * @param User|int $subdomain
     * @param int $pluginId
     * @param boolean $isEnabled
     * @return bool
     */
    public function setPlugin($subdomain, $pluginId, $isEnabled);

    /**
     * Find user by subdomain
     *
     * @param string $name
     * @return User
     */
    public function findBySubdomain($name);

        
}
