<?php namespace Enzaime\Subdomain\Repositories;

use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;

use Enzaime\Subdomain\Repositories\Contracts\SubdomainRepositoryContract;

class SubdomainRepository extends EloquentBaseRepository implements SubdomainRepositoryContract
{
    /**
     * @param array $data
     * @return int
     */
    public function createSubdomain(array $data)
    {
        return $this->create($data);
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function createOrUpdateSubdomain($id, array $data)
    {
        return $this->createOrUpdate($id, $data);
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function updateSubdomain($id, array $data)
    {
        return $this->update($id, $data);
    }

    /**
     * @param int|BaseModelContract|array $id
     * @param bool $force
     * @return bool
     */
    public function deleteSubdomain($id, $force = false)
    {
        return $this->delete($id, $force);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function findByName($name) 
    {
        return $this->findWhere(['username' => $name]);
    }



     /**
     * @param boolean $onlyEnabled
     *
     * @return collection
     */
    public function getPlugins($subdomain) 
    {
        if ($subdomain && !$subdomain instanceof BaseModelContract) {
            $subdomain = $this->findByName($subdomain);
        }

        if (!$subdomain || !count($subdomain->plugins)) {
            return collect([]);
        }

        return $subdomain->plugins()->wherePivot('enabled', true)->get();
       
    }

      /**
     * @param User|int $subdomain
     * @param int $pluginId
     * @param boolean $isEnabled
     * @return bool
     */
    public function setPlugin($subdomain, $pluginId, $isEnabled)
    {
        if (!$subdomain instanceof BaseModelContract) {
            $subdomain = $this->findByName($subdomain) ?: $this->find($subdomain);
        }
        
        return $subdomain->plugins()
            ->syncWithoutDetaching([$pluginId => ['enabled' => $isEnabled]]);
    }

     /**
     * Find user by subdomain
     *
     * @param string $name
     * @return User
     */
    public function findBySubdomain($name) 
    {
        if (!$name) {
            return null;
        }
        return $this->findWhere(['username' => $name]);
    }

      /**
     * @param User|int $subdomain
     * @param int $themeId
     * @return bool
     */
    public function setTheme($subdomain, $themeId)
    {
        if (!$subdomain instanceof BaseModelContract) {
            $subdomain = $this->findByName($subdomain) ?: $this->find($subdomain);
        }
        
        return $subdomain->theme()
            ->sync([$themeId]);
    }
     /**
     * @param User|int $subdomain
     * @param int $themeId
     * @return bool
     */
    public function resetTheme($subdomain, $themeId)
    {
        if (!$subdomain instanceof BaseModelContract) {
            $subdomain = $this->findByName($subdomain) ?: $this->find($subdomain);
        }
        
        return $subdomain->theme()
            ->detach([$themeId]);
    }

    /**
     * Return current subdomain theme 
     * If current theme does not exist then set default-theme as current theme 
     * 
     * @param User|int $subdomain
     * @param int $themeId
     * @return bool
     */
    public function getCurrentTheme($subdomain)
    {   
        if (!$subdomain) {
            return  get_current_theme();
          }     

        if ($subdomain && !$subdomain instanceof BaseModelContract) {
            $subdomain = $this->findByName($subdomain) ?: $this->find($subdomain);
        }

        if (!$subdomain) {
            return null;
        }

        $theme = $subdomain->theme->first()  ;

        // if (!$theme) {
        //     $this->setTheme($subdomain, '2');
        //     //Lazy loading 
        //     $theme = $subdomain->theme()->first();
        // }

        return $theme;
    }
}
