<?php namespace Enzaime\Subdomain\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepositoryCacheDecorator;

use Enzaime\Subdomain\Repositories\Contracts\SubdomainRepositoryContract;
use Enzaime\Base\Models\Contracts\BaseModelContract;

class SubdomainRepositoryCacheDecorator extends EloquentBaseRepositoryCacheDecorator implements SubdomainRepositoryContract
{
    /**
     * @param array $data
     * @return int
     */
    public function createSubdomain(array $data)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function createOrUpdateSubdomain($id, array $data)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param int|null|BaseModelContract $id
     * @param array $data
     * @return int
     */
    public function updateSubdomain($id, array $data)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param int|BaseModelContract|array $id
     * @param bool $force
     * @return bool
     */
    public function deleteSubdomain($id, $force = false)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }
}
