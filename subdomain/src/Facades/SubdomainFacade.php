<?php namespace Enzaime\Subdomain\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Subdomain\Support\Subdomain;

class SubdomainFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        
        return Subdomain::class;
    }
}
