<?php namespace Enzaime\Subdomain\Support;

use Enzaime\Subdomain\Repositories\Contracts\SubdomainRepositoryContract;

class Subdomain 
{
    protected $name = null;
    protected $repository;
    protected $subdomain = null;

    public function __construct(SubdomainRepositoryContract $repository)
    {
        $this->repository =  $repository;
        $parts = explode('.', request()->getHost());

        if (env('ENABLE_SUBDOMAIN', true) && count($parts) > 2) {
            $this->setName(array_first($parts));
        }
    }

    private function setName($name)
    {
        if (!$name) {
            return;
        }
        $this->name = $name;

        $this->subdomain = $this->repository->findByName($name);
    }

    public function getSubdomain()
    {
        return $this->subdomain;
    }


    public function getSubdomainName()
    {
        return $this->name;
    }

    public function getCurrentTheme()
    {
        $theme = $this->repository->getCurrentTheme($this->name); 
        if (! $theme) {
            return null;
        }
        $themeList = get_all_theme_information(false);

        return  $themeList->where('id', $theme->id)->first(); 
    }

}
