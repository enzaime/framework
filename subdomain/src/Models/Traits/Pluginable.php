<?php namespace Enzaime\Subdomain\Models\Traits;

use Enzaime\Base\ModulesManagement\Models\Plugins;


trait Pluginable {
    public function plugins()
    {
        return $this->belongsToMany(Plugins::class, 'user_plugin', 'user_id', 'plugin_id')
            ->withPivot('enabled')
            ->withTimestamps();
    }

}