<?php namespace Enzaime\Subdomain\Models\Traits;

use Enzaime\Base\ThemesManagement\Models\Theme;

trait Themable 
{

    public function theme()
    {
        return $this->belongsToMany(Theme::class, 'subdomain_theme', 'subdomain_id', 'theme_id');
    }
}