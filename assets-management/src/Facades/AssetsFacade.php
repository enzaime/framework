<?php namespace Enzaime\Base\AssetsManagement\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\AssetsManagement\Assets;

class AssetsFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Assets::class;
    }
}
