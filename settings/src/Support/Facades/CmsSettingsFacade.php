<?php namespace Enzaime\Base\Settings\Support\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Settings\Support\CmsSettings;

class CmsSettingsFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CmsSettings::class;
    }
}
