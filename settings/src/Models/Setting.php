<?php namespace Enzaime\Base\Settings\Models;

use Enzaime\Base\Models\EloquentBase as BaseModel;
use Enzaime\Base\Settings\Models\Contracts\SettingModelContract;

class Setting extends BaseModel implements SettingModelContract
{
    protected $table = 'settings';

    protected $primaryKey = 'id';

    protected $subdomain = true;

    protected $fillable = [
        'option_key',
        'option_value',
        'subdomain_id',
    ];
}
