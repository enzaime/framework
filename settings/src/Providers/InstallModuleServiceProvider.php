<?php namespace Enzaime\Base\Settings\Providers;

use Illuminate\Support\ServiceProvider;

class InstallModuleServiceProvider extends ServiceProvider
{
    protected $module = ENZAIME_SETTINGS;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        acl_permission()
            ->registerPermission('View settings page', 'view-settings', $this->module)
            ->registerPermission('Edit settings', 'edit-settings', $this->module);
    }
}
