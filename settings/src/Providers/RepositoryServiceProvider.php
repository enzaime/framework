<?php namespace Enzaime\Base\Settings\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Settings\Models\Setting;
use Enzaime\Base\Settings\Repositories\SettingRepository;
use Enzaime\Base\Settings\Repositories\Contracts\SettingRepositoryContract;
use Enzaime\Base\Settings\Repositories\SettingRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SettingRepositoryContract::class, function () {
            $repository = new SettingRepository(new Setting);

            if (config('enzaime-caching.repository.enabled')) {
                return new SettingRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
