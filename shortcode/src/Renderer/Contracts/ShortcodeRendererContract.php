<?php namespace Enzaime\Base\Shortcode\Renderer\Contracts;

interface ShortcodeRendererContract
{
    /**
     * @var \Enzaime\Base\Shortcode\Compilers\Shortcode $shortcode
     * @var string $content
     * @var \Enzaime\Base\Shortcode\Compilers\ShortcodeCompiler $compiler
     * @var string $name
     */
    public function handle($shortcode, $content, $compiler, $name);
}
