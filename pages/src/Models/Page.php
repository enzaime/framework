<?php namespace Enzaime\Base\Pages\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Enzaime\Base\Models\EloquentBase as BaseModel;
use Enzaime\Base\Pages\Models\Contracts\PageModelContract;
use Enzaime\Base\Users\Models\User;

class Page extends BaseModel implements PageModelContract
{
    use SoftDeletes;

    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $subdomain = true;

    protected $fillable = [
        'title', 'page_template', 'slug', 'description', 'content', 'subdomain_id', 'thumbnail', 'keywords', 'status', 'order',
        'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'created_by')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modifier()
    {
        return $this->belongsTo(User::class, 'updated_by')->withTrashed();
    }
}
