<?php namespace Enzaime\Base\Pages\Hook\Actions;

use Enzaime\Base\Pages\Repositories\Contracts\PageRepositoryContract;
use Enzaime\Base\Pages\Repositories\PageRepository;

class RegisterDashboardStats
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(PageRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function handle()
    {
        $count = $this->repository->count();
        echo view('enzaime-pages::admin.dashboard-stats.stat-box', [
            'count' => $count
        ]);
    }
}
