<?php namespace Enzaime\Base\Pages\Http\Requests;

use Enzaime\Base\Http\Requests\Request;
use Enzaime\Base\Rules\UniqueWith;

class UpdatePageRequest extends Request
{
    public function rules()
    {
        $uniqueWith = (new UniqueWith())->table('pages')
            ->fields(['subdomain_id' => get_subdomain_id()])
            ->attributeAs('slug')
            ->except(request()->route()->parameter('id'));

        return [
            'page.page_template' => 'string|max:255|nullable',
            'page.title' => 'string|max:255|required',
            'page.slug' => ['string', 'max:255', 'nullable', $uniqueWith], //|unique:' . enzaime_db_prefix() . 'pages,slug,' . request()->route()->parameter('id'),
            'page.description' => 'string|max:1000|nullable',
            'page.content' => 'string|nullable',
            'page.thumbnail' => 'string|max:255|nullable',
            'page.keywords' => 'string|max:255|nullable',
            'page.status' => 'required',
            'page.order' => 'integer|min:0',
        ];
    }
}
