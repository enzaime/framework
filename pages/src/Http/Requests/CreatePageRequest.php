<?php namespace Enzaime\Base\Pages\Http\Requests;

use Enzaime\Base\Rules\UniqueWith;
use Enzaime\Base\Http\Requests\Request;

class CreatePageRequest extends Request
{
    public function rules()
    {
        $uniqueWith = (new UniqueWith())->table('pages')
            ->fields(['subdomain_id' => get_subdomain_id()])
            ->attributeAs('slug');

        return [
            'page.page_template' => 'string|max:255|nullable',
            'page.title' => 'string|max:255|required',
            'page.slug' => ['string', 'max:255', 'nullable', $uniqueWith], //|unique:' . enzaime_db_prefix() . 'pages,slug',
            'page.description' => 'string|max:1000|nullable',
            'page.content' => 'string|nullable',
            'page.thumbnail' => 'string|max:255|nullable',
            'page.keywords' => 'string|max:255|nullable',
            'page.status' => 'required',
            'page.order' => 'integer|min:0',
        ];
    }
}
