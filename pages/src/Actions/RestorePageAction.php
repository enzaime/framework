<?php namespace Enzaime\Base\Pages\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\Pages\Repositories\Contracts\PageRepositoryContract;
use Enzaime\Base\Pages\Repositories\PageRepository;

class RestorePageAction extends AbstractAction
{
    /**
     * @var PageRepository
     */
    protected $pageRepository;

    public function __construct(PageRepositoryContract $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param $id
     * @return array
     */
    public function run($id)
    {
        $id = do_filter(BASE_FILTER_BEFORE_RESTORE, $id, ENZAIME_PAGES);

        $result = $this->pageRepository->restore($id);

        do_action(BASE_ACTION_AFTER_RESTORE, ENZAIME_PAGES, $id, $result);

        $msg = $result ? trans('enzaime-core::base.form.request_completed') : trans('enzaime-core::base.form.error_occurred');

        if (!$result) {
            return $this->error($msg);
        }

        return $this->success($msg);
    }
}
