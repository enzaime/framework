<?php namespace Enzaime\Base\Pages\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\Pages\Repositories\Contracts\PageRepositoryContract;
use Enzaime\Base\Pages\Repositories\PageRepository;

class CreatePageAction extends AbstractAction
{
    /**
     * @var PageRepository
     */
    protected $pageRepository;

    public function __construct(PageRepositoryContract $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    public function run(array $data)
    {
        do_action(BASE_ACTION_BEFORE_CREATE, ENZAIME_PAGES, 'create.post');

        $data['created_by'] = get_current_logged_user_id();

        $result = $this->pageRepository->createPage($data);

        do_action(BASE_ACTION_AFTER_CREATE, ENZAIME_PAGES, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
