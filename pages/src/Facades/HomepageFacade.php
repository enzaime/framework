<?php namespace Enzaime\Base\Pages\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Pages\Support\HomepageSupport;

class HomepageFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return HomepageSupport::class;
    }
}
