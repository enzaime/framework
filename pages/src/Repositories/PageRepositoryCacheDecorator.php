<?php namespace Enzaime\Base\Pages\Repositories;

use Enzaime\Base\Pages\Models\Page;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepositoryCacheDecorator;
use Enzaime\Base\Pages\Repositories\Contracts\PageRepositoryContract;
use Enzaime\Base\Repositories\Traits\EloquentUseSoftDeletesCache;

class PageRepositoryCacheDecorator extends EloquentBaseRepositoryCacheDecorator implements PageRepositoryContract
{
    use EloquentUseSoftDeletesCache;

    /**
     * @param array $data
     * @return int
     */
    public function createPage(array $data)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param Page|int $id
     * @param array $data
     * @return int
     */
    public function updatePage($id, array $data)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param int|array $ids
     * @param bool $force
     * @return bool
     */
    public function deletePage($ids, $force = false)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getPages(array $params)
    {
        return $this->beforeGet(__FUNCTION__, func_get_args());
    }
}
