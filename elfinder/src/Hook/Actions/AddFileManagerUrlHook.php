<?php namespace Enzaime\Base\Elfinder\Hook\Actions;

class AddFileManagerUrlHook
{
    public function __construct()
    {

    }

    public function execute()
    {
        echo view('enzaime-elfinder::admin.hook.top-script')->render();
    }
}
