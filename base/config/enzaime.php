<?php

/**
 * Global config for Enzaime
 * @author Tedozi Manson <duyphan.developer@gmail.com>
 */
return [
    /**
     * Admin route slug
     */
    'admin_route' => env('ENZAIME_ADMIN_ROUTE', 'admincp'),

    'api_route' => env('ENZAIME_API_ROUTE', 'api'),

    'languages' => [
        'vi' => 'Vietnamese',
        'en' => 'English'
    ],

    'external_core' => [
        Enzaime\Base\Elfinder\Providers\ModuleProvider::class,
        Enzaime\Base\Pages\Providers\ModuleProvider::class,
        Enzaime\Base\CustomFields\Providers\ModuleProvider::class,
        Enzaime\Base\StaticBlocks\Providers\ModuleProvider::class,
    ],
];
