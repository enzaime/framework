<?php namespace NewsTV\Http\ViewComposers;

use Illuminate\View\View;
use Enzaime\Base\Criterias\Filter\WithViewTracker;
use Enzaime\Plugins\Blog\Repositories\Contracts\PostRepositoryContract;
use Enzaime\Plugins\Blog\Repositories\PostRepository;

class SidebarViewComposer
{
    /**
     * @var PostRepository
     */
    protected $postRepository;

    public function __construct(PostRepositoryContract $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $postTopViews = $this->postRepository
            ->pushCriteria(new WithViewTracker($this->postRepository->getModel(), ENZAIME_BLOG_POSTS))
            ->advancedGet([
                'condition' => [
                    enzaime_db_prefix() . 'posts.status' => 1,
                ],
                'order_by' => [
                    'view_count' => 'DESC',
                ],
                'select' => [
                    enzaime_db_prefix() . 'posts.title',
                    enzaime_db_prefix() . 'posts.slug',
                    enzaime_db_prefix() . 'posts.id',
                    enzaime_db_prefix() . 'posts.thumbnail',
                    enzaime_db_prefix() . 'view_trackers.count as view_count'
                ],
                'take' => 5,
            ]);

        $popularVideos = get_posts([
            'condition' => [
                'page_template' => 'video',
                'status' => 1
            ],
            'order_by' => [
                'is_featured' => 'DESC',
                'created_at' => 'DESC',
            ],
            'select' => ['created_at', 'title', 'slug', 'id', 'thumbnail'],
            'take' => 4
        ]);

        $view->with('popularVideos', $popularVideos)
            ->with('postTopViews', $postTopViews);
    }
}
