<?php namespace NewsTV\Http\Controllers\Pages;

use NewsTV\Criterias\SearchPostsCriteria;
use NewsTV\Http\Controllers\AbstractController;
use Enzaime\Plugins\Blog\Repositories\Contracts\PostRepositoryContract;
use Enzaime\Plugins\Blog\Repositories\PostRepository;

class SearchController extends AbstractController
{
    /**
     * @var PostRepository
     */
    protected $repository;

    public function __construct(PostRepositoryContract $repository)
    {
        parent::__construct();

        $this->repository = $repository;

        $this->setPageTitle('Search');
    }

    public function index()
    {
        $k = $this->request->input('k');

        $this->dis['posts'] = $this->repository
            ->pushCriteria(new SearchPostsCriteria($k))
            ->advancedGet([
                'condition' => [
                    enzaime_db_prefix() . 'posts.status' => 1,
                ],
                'select' => [
                    enzaime_db_prefix() . 'posts.id',
                    enzaime_db_prefix() . 'posts.title',
                    enzaime_db_prefix() . 'posts.slug',
                    enzaime_db_prefix() . 'posts.description',
                    enzaime_db_prefix() . 'posts.created_at',
                    enzaime_db_prefix() . 'posts.thumbnail',
                ],
                'paginate' => [
                    'per_page' => get_theme_option('items_per_page', 9),
                    'current_paged' => ($this->request->input('page') ?: 1),
                ],
            ]);

        return $this->view('front.page-templates.search');
    }
}
