<?php namespace NewsTV\Criterias;

use Illuminate\Database\Eloquent\Builder;
use Enzaime\Base\Criterias\AbstractCriteria;
use Enzaime\Base\Repositories\AbstractBaseRepository;
use Enzaime\Base\Repositories\Contracts\AbstractRepositoryContract;
use Enzaime\Plugins\Blog\Models\Post;

class SearchPostsCriteria extends AbstractCriteria
{
    /**
     * @var string
     */
    protected $keyword;

    public function __construct($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @param Post|Builder $model
     * @param AbstractRepositoryContract $repository
     * @return mixed
     */
    public function apply($model, AbstractRepositoryContract $repository)
    {
        return $model
            ->where(function ($query) {
                return $query
                    ->where(enzaime_db_prefix() . 'posts.title', 'LIKE', '%' . $this->keyword . '%')
                    ->orWhere(enzaime_db_prefix() . 'posts.slug', 'LIKE', '%' . $this->keyword . '%')
                    ->orWhere(enzaime_db_prefix() . 'posts.keywords', 'LIKE', '%' . $this->keyword . '%');
            })
            ->distinct();
    }
}
