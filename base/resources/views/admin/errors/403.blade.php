@extends('enzaime-core::admin._master')

@section('head')

@endsection

@section('js')

@endsection

@section('js-init')

@endsection

@section('content')
    <div class="layout-1columns">
        <div class="column main">
            <h2 class="headline">403</h2>
            <h3>{{ trans('enzaime-core::errors.' . Constants::FORBIDDEN_CODE . '.title') }}</h3>
            <p>{{ trans('enzaime-core::errors.' . Constants::FORBIDDEN_CODE . '.message') }}</p>
        </div>
    </div>
@endsection
