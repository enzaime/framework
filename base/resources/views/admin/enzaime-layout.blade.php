<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <base href="{{ asset('') }}">

    <meta charset="utf-8"/>
    <title>{{ $pageTitle or 'Dashboard' }} | {{ config('app.name', 'Enzaime') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="{{ config('app.name', 'Enzaime') }}" name="description"/>

    {!! assets_management()->renderStylesheets() !!}

    @php do_action(BASE_ACTION_HEADER_CSS) @endphp

    <link rel="stylesheet" href="{{ asset('admin/theme/lte/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/theme/lte/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">

    <link rel="shortcut icon" href="{{ get_setting('favicon', 'favicon.png') }}"/>

  
    {!! assets_management()->renderScripts('top') !!}

    <script type="text/javascript">
        var BASE_URL = '{{ asset('') }}';
    </script>

    @php do_action(BASE_ACTION_HEADER_JS) @endphp


    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/webfont.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/climacons-font.css') }}">
    {{--  <link rel="stylesheet" href="{{ asset('admin/enzaime/vendor/bootstrap/dist/css/bootstrap.css') }}">  --}}
    {{--  <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/font-awesome.css') }}">  --}}
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/card.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/sli.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/app.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/enzaime/styles/app.skins.css') }}">
    <style>
      .modal-backdrop, .modal-backdrop.fade.in {
        z-index: -1000;
      }
      .form-group select.form-control {
        width: 100%;
      }
    </style>
    
    @stack('head')

    @yield('head')

</head>


<body>
@section('body')
  @section('page-loading')
    <!--page loading spinner -->
    @if(config('enzaime-templates.page_loading'))
      <div class="pageload">
        <div class="pageload-inner">
          <div class="sk-rotating-plane"></div>
        </div>
      </div>
    @endif
  @show
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
    <!-- sidebar panel -->
    @include('enzaime-core::admin._partials.enzaime.sidebar')
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">
      <!-- top header -->
      @include('enzaime-core::admin._partials.enzaime.header')
      <!-- /top header -->
      <!-- main area -->
          
      <div class="main-content">     
            {{--  <section class="content-header">
            @include('enzaime-core::admin._partials.page-title')
            @include('enzaime-core::admin._partials.breadcrumbs')
        
            </section>   --}}
        @section('flash-message')
          @include('enzaime-core::admin._partials.flash-messages')
        @show
        
        @yield('content')


      @include('enzaime-core::admin._partials.enzaime.footer')
        
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->
    <!-- bottom footer -->
    {{--  @include('enzaime-core::admin._partials.enzaime.footer')  --}}
    @include('enzaime-core::admin._partials.control-sidebar')
      
    <!-- /bottom footer -->
    <!-- chat -->
  
    <!-- /chat -->
  </div>



   {{--Modals--}}
   @include('enzaime-core::admin._partials.modals')
@show
<!--[if lt IE 9]>
    <script src="admin/plugins/respond.min.js"></script>
    <script src="admin/plugins/excanvas.min.js"></script>
    <![endif]-->

  <!-- endbuild -->
  <!-- page scripts -->
 <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="{{ asset('admin/enzaime/scripts/helpers/sameheight.js') }}"></script>
  {{--  <script src="{{ asset('admin/enzaime/scripts/ui/dashboard.js"></sc') }}ript>  --}}
    {{--BEGIN plugins--}}
    <script src="{{ asset('admin/theme/lte/js/app.js') }}"></script>
    <script src="{{ asset('admin/js/enzaime-core.js') }}"></script>
    <script src="{{ asset('admin/theme/lte/js/demo.js') }}"></script>
    {!! assets_management()->renderScripts('bottom') !!}
    {{--END plugins--}}

    @php do_action(BASE_ACTION_FOOTER_JS) @endphp

    <script src="{{ asset('admin/js/script.js') }}"></script>

     <!-- build:js({.tmp,app}) scripts/app.min.js -->

      <script src="{{ asset('admin/enzaime/scripts/helpers/modernizr.js') }}"></script>
  {{--  <script src="{{ asset('admin/enzaime/vendor/jquery/dist/jquery.js"></sc') }}ript>  --}}
  {{--  <script src="{{ asset('admin/enzaime/vendor/bootstrap/dist/js/bootstrap.js"></sc') }}ript>  --}}
  <script src="{{ asset('admin/enzaime/vendor/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ asset('admin/enzaime/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
  <script src="{{ asset('admin/enzaime/scripts/helpers/smartresize.js') }}"></script>
  <script src="{{ asset('admin/enzaime/scripts/constants.js') }}"></script>
  <script src="{{ asset('admin/enzaime/scripts/main.js') }}"></script>


    @stack('js')

    @yield('js')

    @stack('js-init')

    @yield('js-init')
</body>

</html>
