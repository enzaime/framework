<footer class="main-footer">
    <div class="pull-right">
        <b>{{ trans('enzaime-core::base.version') }}</b> {{ get_cms_version() }}
    </div>
    2016 ~ {{ date('Y') }} &copy; <strong>Enzaime CMS</strong> by <a href="https://sgsoft-studio.com" target="_blank">sgsoft-studio.com</a>. All rights reserved.
</footer>
