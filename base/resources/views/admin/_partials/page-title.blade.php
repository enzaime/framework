<h1 class="page-title">
    {{ $pageTitle or 'Enzaime Dashboard' }}
    <small>{{ $subPageTitle or '' }}</small>
</h1>
