<?php
/**
 * @var \Illuminate\Support\Collection $links
 */
?>
@foreach($links as $key => $link)
      
    @php
        $hasChildren = ($link['children']->count()) ? true : false;
        $hasBadge = isset($link['badge']) ? true : false;
        $numberOfBadge = isset($link['number_of_badge']) ?  $link['number_of_badge'] : 0;
    @endphp

    @if(has_permissions($loggedInUser, $link['permissions']))
        <li class="{{ (in_array($link['id'], $active)) ? 'open' : '' }} ">
            <a href="{{ $link['link'] or 'javascript:;' }}" id="{{ $link['id'] }}">
                @if($hasBadge)
                    <span class="badge pull-right">{{$numberOfBadge}}</span>
                @endif
                
                <i class="{{ isset($link['font_icon']) && $link['font_icon'] ? $link['font_icon'] . ' icon-padding' : '' }}"></i>
                
                <span>{{ $link['title'] or '' }}</span>
            </a>
            @if($hasChildren)
                <ul class="sub-menu">
                    @include('enzaime-core::admin._partials.enzaime.dashboard-menu', [
                        'links' => $link['children'],
                        'isChildren' => true,
                        'level' => ($level + 1),
                        'active' => $active,
                    ])
                </ul>
            @endif
        </li>
    @endif
    
@endforeach
