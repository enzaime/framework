  <div class="header navbar">
      <div class="brand visible-xs">
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a class="brand-logo">
          <span>{{config('app.name')}}</span>
        </a>
        <!-- /logo -->
      </div>
      <ul class="nav navbar-nav hidden-xs">
        <li>
          <a href="javascript:;" class="small-sidebar-toggle ripple" data-toggle="layout-small-menu">
            <i class="icon-toggle-sidebar"></i>
          </a>
        </li>
        <li class="searchbox">
          <a href="javascript:;" data-toggle="search">
            <i class="search-close-icon icon-close hide"></i>
            <i class="search-open-icon icon-magnifier"></i>
          </a>
        </li>
        <li class="navbar-form search-form hide">
          <input type="search" class="form-control search-input" placeholder="Start typing...">
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right hidden-xs">
        <li>
          <a href="javascript:;" class="ripple" data-toggle="dropdown">
            <span>EN</span>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="javascript:;">English</a>
            </li>
            <li>
              <a href="javascript:;">Russian</a>
            </li>
            <li>
              <a href="javascript:;">French</a>
            </li>
            <li>
              <a href="javascript:;">Spanish</a>
            </li>
          </ul>
        </li>
      
          @if (isset($loggedInUser))
            <li>
          <a href="javascript:;" class="ripple" data-toggle="dropdown">
            <i class="icon-bell"></i>
          </a>
          <ul class="dropdown-menu notifications">
            <li class="notifications-header">
              <p class="text-muted small">You have 3 new messages</p>
            </li>
            <li>
              <ul class="notifications-list">
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-success text-white">
                        <i class="icon-bulb"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Sean</b> launched a new application</span>
                    <span class="time">2s</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-danger text-white">
                        <i class="icon-cursor"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Removed calendar</b> from app list</span>
                    <span class="time">4h</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-primary text-white">
                        <i class="icon-basket"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Denise</b> bought <b>Urban Admin Kit</b></span>
                    <span class="time">2d</span>
                  </a>
                </li>
                <li>
                  <a href="javascript:;">
                    <div class="notification-icon">
                      <div class="circle-icon bg-info text-white">
                        <i class="icon-bubble"></i>
                      </div>
                    </div>
                    <span class="notification-message"><b>Vincent commented</b> on an item</span>
                    <span class="time">2s</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="javascript:;" class="ripple" data-toggle="dropdown">
           
             <img 
              class="header-avatar img-circle"
               src="{{ isset($loggedInUser->avatar) ? get_image($loggedInUser->avatar) : get_image(null) }}"
               alt="{{$loggedInUser->name}}"
               title="{{$loggedInUser->name}}"
               >
                         
            <span>{{$loggedInUser->name}}</span>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
           
            <li>
             <a href="{{ route('admin::auth.logout.get') }}"> {{ trans('enzaime-users::auth.sign_out') }} </a>
            </li>
          </ul>
        </li>
        @endif
        <li>
          <a href="javascript:;" class="ripple" data-toggle="layout-chat-open">
            <i class="icon-us"></i>
          </a>
        </li>
      </ul>
  </div>