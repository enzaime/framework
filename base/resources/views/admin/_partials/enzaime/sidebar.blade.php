    <div class="sidebar-panel offscreen-left">
      <div class="brand">
        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-apps hidden-xs" data-toggle="quick-launch">
          <i class="icon-grid"></i>
        </a>
        <!-- /toggle small sidebar menu -->
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a class="brand-logo" href="{{url(config('enzaime.admin_route'))}}">
          <img class="img-responsive" src="{{get_setting('site_logo') ?: config('app.logo')}}" alt="">
        </a>
        <a href="#" class="small-menu-visible brand-logo">E</a>
        <!-- /logo -->
      </div>
      @if(isset($loggedInUser))
        <ul class="quick-launch-apps hide">
            @php
              $bgClass = ['bg-success', 'bg-info', 'bg-primary', 'bg-danger'];
              srand(5); 
            @endphp
            @foreach(admin_quick_link()->get() as $type => $linkData)
              <li>
                  <a href="{{ $linkData['url'] }}">
                    <span class="app-icon {{$bgClass[rand(0, 1000)%4]}} text-white">
                      @if(array_get($linkData, 'icon'))
                          <i class="ion {{ $linkData['icon'] }}"></i>
                          @else
                          {{ $linkData['title'][0]}}
                      @endif
                  </span>
                      
                      <span class="app-title"> {{ $linkData['title'] }}</span>
                  </a>
              </li>
              @endforeach
        
        </ul>
        <!-- main navigation -->
        <nav role="navigation">
          <ul class="nav">
          {!! dashboard_menu()->render('enzaime-core::admin._partials.enzaime.dashboard-menu') !!}
          </ul>
        </nav>
      @endif
      
      <!-- /main navigation -->
    </div>