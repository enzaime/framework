$(document).ready(function(){
    /**
     * Detect IE
     */
    Enzaime.isIE(function(){
        /**
         * Callback
         */
    });

    /**
     * Add csrf token to ajax request
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    });

    /**
     * Handle select media box
     */
    Enzaime.handleSelectMediaBox();

    Enzaime.tabChangeUrl();

    /**
     * Init layout
     */
    Enzaime.initAjax();

    Enzaime.fixedTopFormActions();
});

$(window).load(function () {
    Enzaime.hideLoading();
});
