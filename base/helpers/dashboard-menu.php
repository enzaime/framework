<?php

if (!function_exists('dashboard_menu')) {
    /**
     * @return \Enzaime\Base\Menu\Support\DashboardMenu
     */
    function dashboard_menu()
    {
        return \Enzaime\Base\Menu\Facades\DashboardMenuFacade::getFacadeRoot();
    }
}
