<?php

if (!function_exists('admin_bar')) {
    /**
     * @return \Enzaime\Base\Support\AdminBar
     */
    function admin_bar()
    {
        return \Enzaime\Base\Facades\AdminBarFacade::getFacadeRoot();
    }
}
