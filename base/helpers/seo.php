<?php

if (!function_exists('seo')) {
    /**
     * @return \Enzaime\Base\Support\SEO
     */
    function seo()
    {
        return \Enzaime\Base\Facades\SeoFacade::getFacadeRoot();
    }
}
