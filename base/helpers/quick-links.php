<?php

if (!function_exists('admin_quick_link')) {
    /**
     * @return \Enzaime\Base\Support\AdminQuickLink
     */
    function admin_quick_link()
    {
        return \Enzaime\Base\Facades\AdminQuickLinkFacade::getFacadeRoot();
    }
}