<?php

if (!function_exists('enzaime_datatable')) {
    /**
     * @return \Yajra\DataTables\DataTables
     */
    function enzaime_datatable()
    {
        return \Yajra\DataTables\Facades\DataTables::getFacadeRoot();
    }
}
