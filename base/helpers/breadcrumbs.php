<?php

if (!function_exists('breadcrumbs')) {
    /**
     * @return \Enzaime\Base\Support\Breadcrumbs
     */
    function breadcrumbs()
    {
        return \Enzaime\Base\Facades\BreadcrumbsFacade::getFacadeRoot();
    }
}