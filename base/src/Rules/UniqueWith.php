<?php

namespace Enzaime\Base\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueWith implements Rule
{
    protected $fields = [];
    protected $table = '';
    protected $exceptField = 'id';
    protected $except;
    protected $attribute = null;

    public function __construct($table = '', $fields = [], $except = null, $attribute = null, $exceptField = 'id')
    {
        $this->fields = $fields;
        $this->table = $table;
        $this->except = $except;
        $this->exceptField = $exceptField;
        $this->attribute = $attribute;
    }
    
    public function table($name)
    {
        $this->table = $name;
        
        return $this;
    }

    public function fields($nameOrNameValue, $value = null)
    {
        if (! is_array($nameOrNameValue)) {
            
            $nameOrNameValue = [$nameOrNameValue => $value ?: request()->{$nameOrNameValue}];
        }
        $this->fields = array_merge($this->fields, $nameOrNameValue);

        return $this;
    }

    public function except($id, $exceptField = null)
    {
        if ($exceptField) {

            $this->exceptField = $exceptField;
        }
        $this->except = $id;

        return $this;

    }

    public function attributeAs($name) 
    {
        $this->attribute = $name;

        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $attribute = $this->getAttributeAs($attribute);
        $query = \DB::table($this->table)->where($attribute, $value);

        foreach($this->fields as $key => $value) {
            $query = $query->where($key, $value);
        }
        if ($this->except) {
            $query = $query->where($this->exceptField, '!=', $this->except);
        }
    
        return $query->count() == 0;
    }

    public function getAttributeAs($attribute = null)
    {
        if ($this->attribute) {
            return $this->attribute;
        }

        return $attribute;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute already exists';
    }

}