<?php namespace Enzaime\Base\Http\Controllers;

class DashboardController extends BaseAdminController
{
    protected $module = 'enzaime-core';

    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        $this->setPageTitle(trans('enzaime-core::stats.dashboard_statistics'));
        $this->getDashboardMenu('enzaime-dashboard');
        return do_filter(BASE_FILTER_CONTROLLER, $this, ENZAIME_DASHBOARD_STATS)->viewAdmin('dashboard');
    }
}
