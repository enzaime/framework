<?php namespace Enzaime\Base\Http\Controllers;

use Enzaime\Base\Facades\DashboardLanguageFacade;

class DashboardLanguageController extends BaseController
{
    protected $module = 'enzaime-core';

    /**
     * @param $languageSlug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getChangeLanguage($languageSlug)
    {
        DashboardLanguageFacade::setDashboardLanguage($languageSlug);

        return redirect()->back();
    }
}
