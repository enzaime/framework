<?php namespace Enzaime\Base\Models;

use Enzaime\Base\Models\Contracts\ViewTrackerModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class ViewTracker extends BaseModel implements ViewTrackerModelContract
{
    protected $table = 'view_trackers';

    protected $primaryKey = 'id';

    protected $fillable = ['entity', 'entity_id', 'count'];

    public $timestamps = false;
    public $subdomain = false;
    
    /**
     * Count getter
     * @param $value
     * @return int
     */
    public function getCountAttribute($value)
    {
        return (int)$value;
    }
}
