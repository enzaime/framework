<?php namespace Enzaime\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Subdomain\Scopes\SubdomainScope;

abstract class EloquentBase extends Model implements BaseModelContract
{
    /**
     * Set primary key of model
     * @var string
     */
    protected $primaryKey = false;

    /**
     * @var string
     */
    protected $prefix = ENZAIME_DB_PREFIX;
    /**
     * @var string
     */
    protected $subdomainField = 'subdomain_id';
    /**
     * @var bool
     */
    protected $subdomain = false;

    public function __construct(array $attributes = [])
    {
        if (isset($this->prefix)) {
            $this->table = $this->prefix . $this->table;
        }

        parent::__construct($attributes);
    }


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($modal) {
            if ($modal->subdomain) {
                $modal->{$modal->subdomainField} = subdomain() ? subdomain()->id : null;
            }
        });
        static::addGlobalScope(new SubdomainScope);
    }

    /**
     * Get primary key
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getKeyName();
    }

    /**
     * @param array|string $attribute
     * @return $this
     */
    public function expandFillable($attribute)
    {
        $attributes = is_array($attribute) ? $attribute : func_get_args();

        $this->fillable = array_unique(array_merge($attributes, $this->fillable));

        return $this;
    }

    /**
     * @param array|string $attribute
     * @return $this
     */
    public function unsetFillable($attribute)
    {
        $attributes = is_array($attribute) ? $attribute : func_get_args();

        $this->fillable = array_filter($this->fillable, function ($item) use ($attributes) {
            return !in_array($item, $attributes);
        });

        return $this;
    }

    public function hasSubdomain() 
    {
        return $this->subdomain;
    }
    public function getSubdomainKey() 
    {
        return $this->subdomainField;
    }

    /**
     * This is where to put some scope query
     */
}
