<?php namespace Enzaime\Base\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Models\ViewTracker;
use Enzaime\Base\Repositories\Contracts\ViewTrackerRepositoryContract;
use Enzaime\Base\Repositories\ViewTrackerRepository;
use Enzaime\Base\Repositories\ViewTrackerRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ViewTrackerRepositoryContract::class, function () {
            $repository = new ViewTrackerRepository(new ViewTracker());

            if (config('enzaime-caching.repository.enabled')) {
                return new ViewTrackerRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
