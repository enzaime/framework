<?php namespace Enzaime\Base\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Hook\StoreSettingsHook;

class HookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        add_filter(BASE_FILTER_BEFORE_UPDATE, [StoreSettingsHook::class, 'execute'], 30);
    }
}
