<?php namespace Enzaime\Base\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Http\Middleware\AdminBarMiddleware;
use Enzaime\Base\Http\Middleware\ConstructionModeMiddleware;
use Enzaime\Base\Http\Middleware\CorsMiddleware;

class MiddlewareServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * @var Router $router
         */
        $router = $this->app['router'];

        if(!is_admin_panel()) {
            $router->pushMiddlewareToGroup('web', ConstructionModeMiddleware::class);
            $router->pushMiddlewareToGroup('web', AdminBarMiddleware::class);
            $router->pushMiddlewareToGroup('api', CorsMiddleware::class);
        }
    }
}
