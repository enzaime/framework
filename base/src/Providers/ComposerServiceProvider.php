<?php namespace Enzaime\Base\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Http\ViewComposers\AdminBreadcrumbsViewComposer;
use Enzaime\Base\Http\ViewComposers\BasePartialsViewComposer;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'enzaime-core::admin._partials.breadcrumbs',
        ], AdminBreadcrumbsViewComposer::class);
        view()->composer([
            'enzaime-core::front._admin-bar',
            'enzaime-core::admin._partials.header',
            'enzaime-core::admin._partials.sidebar',
        ], BasePartialsViewComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
