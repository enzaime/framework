<?php namespace Enzaime\Base\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \Enzaime\Base\Console\Commands\InstallCmsCommand::class,
            \Enzaime\Base\Console\Commands\UpdateCmsCommand::class,
            \Enzaime\Base\Console\Commands\BootCmsCommand::class,
        ]);
    }
}
