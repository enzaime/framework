<?php namespace Enzaime\Base\Criterias;

use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Base\Repositories\Contracts\AbstractRepositoryContract;

abstract class AbstractCriteria
{
    /**
     * @param BaseModelContract $model
     * @param AbstractRepositoryContract $repository
     * @return mixed
     */
    abstract public function apply($model, AbstractRepositoryContract $repository);
}
