<?php namespace Enzaime\Base\Criterias\Filter;

use Illuminate\Database\Eloquent\Builder;
use Enzaime\Base\Models\Contracts\BaseModelContract;
use Enzaime\Base\Models\EloquentBase;
use Enzaime\Base\Repositories\Contracts\AbstractRepositoryContract;
use Enzaime\Base\Criterias\AbstractCriteria;

class WithViewTracker extends AbstractCriteria
{
    /**
     * @var BaseModelContract
     */
    protected $relatedModel;

    /**
     * @var string
     */
    protected $screenName;

    public function __construct(BaseModelContract $relatedModel, $screenName)
    {
        $this->relatedModel = $relatedModel;

        $this->screenName = $screenName;
    }

    /**
     * @param EloquentBase|Builder $model
     * @param AbstractRepositoryContract $repository
     * @return mixed
     */
    public function apply($model, AbstractRepositoryContract $repository)
    {
        return $model
            ->leftJoin(enzaime_db_prefix() . 'view_trackers', $this->relatedModel->getTable() . '.' . $this->relatedModel->getPrimaryKey(), '=', enzaime_db_prefix() . 'view_trackers.entity_id')
            ->where(enzaime_db_prefix() . 'view_trackers.entity', '=', $this->screenName);
    }
}
