<?php namespace Enzaime\Base\Console\Commands;

use Illuminate\Console\Command;
use Enzaime\Base\ModulesManagement\Actions\UpdateCoreModuleAction;

class BootCmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:boot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Boot CMS command';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->installPlugins();
        // $this->installThemes();

    }


    protected function installPlugins()
    {
        $plugins = get_plugin();//->where('default', true);

        foreach ($plugins as $plugin) {
            $this->line("Plugin {$plugin['alias']} is installing ...");
            \Artisan::call('plugin:enable', ['alias' => $plugin['alias']]);
            \Artisan::call('plugin:install', ['alias' => $plugin['alias']]);
            $this->info("Plugin {$plugin['alias']} is installed");
            
        }
    }

    protected function installThemes()
    {
        $themes = get_all_theme_information(false);
        foreach($themes  as $theme) {
            $provider = array_get($theme, 'namespace') . '\Providers\ModuleProvider';
            if (!class_exists($provider)) {
                $this->error($provider);
                continue;
            }
            app()->register($provider);
            $this->line("Publishing public assets of  {$theme['alias']} theme");
            

            \Artisan::call('vendor:publish', [
                '--provider' => $provider,
                '--tag' => $theme['alias'],
                '--force' => 'true'
                ]);

            $this->info("Theme {$theme['alias']} is installed");
        }
    }

    protected function resolveDependency(array $module) 
    {
        $installed  = [];
        $enabled  = [];

        foreach (array_get($module, 'require', []) as $pluginAlias => $version) {
            $this->info("Resolving $pluginAlias dependency...");
        
            $plugin = get_plugin($pluginAlias);

            if (! $plugin) {
                continue;
            }
            if (! array_get($plugin, 'enabled', false)) {
                \Artisan::call('plugin:enable', ['alias' => $plugin['alias']]);
                $enabled[] = $plugin['alias'];
            }
            if (! array_get($plugin, 'installed', false)) {
                \Artisan::call('plugin:install', ['alias' => $plugin['alias']]);
                $installed[] = $plugin['alias'];
                
            }
        }

        return ['installed' => $installed, 'enabled' => $enabled];
        
    }
}
