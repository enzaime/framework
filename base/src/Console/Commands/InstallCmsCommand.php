<?php namespace Enzaime\Base\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Enzaime\Base\ACL\Models\Role;
use Enzaime\Base\ACL\Repositories\Contracts\RoleRepositoryContract;
use Enzaime\Base\ACL\Repositories\RoleRepository;
use Enzaime\Base\ModulesManagement\Repositories\Contracts\CoreModuleRepositoryContract;
use Enzaime\Base\ModulesManagement\Repositories\CoreModuleRepository;
use Enzaime\Base\Providers\InstallModuleServiceProvider;
use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;
use Enzaime\Base\Users\Repositories\UserRepository;

class InstallCmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:install {option?} {--fresh=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Enzaime';

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var array
     */
    protected $container = [];

    /**
     * @var array
     */
    protected $dbInfo = [];

    /**
     * @var Role
     */
    protected $roles = ['System Admin', 'Super Admin'];

    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    protected $app;

    /**
     * @var CoreModuleRepository
     */
    protected $coreModuleRepository;

    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        Filesystem $filesystem,
        CoreModuleRepositoryContract $coreModuleRepository,
        RoleRepositoryContract $roleRepository,
        UserRepositoryContract $userRepository
    )
    {
        parent::__construct();

        $this->files = $filesystem;

        $this->app = app();

        $this->coreModuleRepository = $coreModuleRepository;

        $this->roleRepository = $roleRepository;

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
      
        $this->createEnv();
     
        $this->getDatabaseInformation();
        /**
         * Migrate tables
         */
        if (! ($this->option('fresh') === 'false')) {
            $this->line('Migrate database...');
            $this->app->register(InstallModuleServiceProvider::class);

            $this->line('Install module dependencies...');
        }
        $this->registerInstallModuleService();
        

        foreach ($this->roles as $role ) {
            $this->line("Create $role role...");
            $this->findOrCreateRole($role);
        }

        $this->line('Create admin user...');
      
        $this->createAdminUser();

        session()->flush();
        session()->regenerate();
        \Artisan::call('cache:clear');
        \Artisan::call('view:clear');

        $this->info("\nEnzaime installed. Current version is " . get_cms_version());
    }

    /**
     * Get database information
     */
    protected function getDatabaseInformation()
    {
        $this->dbInfo['connection'] = env('DB_CONNECTION');
        $this->dbInfo['host'] = env('DB_HOST');
        $this->dbInfo['database'] = env('DB_DATABASE');
        $this->dbInfo['username'] = env('DB_USERNAME');
        $this->dbInfo['password'] = env('DB_PASSWORD');
        $this->dbInfo['port'] = env('DB_PORT');

        if (!check_db_connection()) {
            $this->error('Please setup your database information first!');
            die();
        }

        $this->info('Database OK...');
    }

    protected function findOrCreateRole($name)
    {
        return $this->roleRepository->findWhereOrCreate(
                ['slug' => str_slug($name)], 
                ['name' => $name]
            );
    }

    protected function createAdminUser()
    {
        try {
            $user = $this->userRepository->find($this->userRepository->create([
                'username' => $this->ask('Your username', 'admin'),
                'user_type' =>'1',
                'name' => $this->ask('Your name', 'Admin'),
                'email' => $this->ask('Your email', 'admin@enz.com'),
                'password' => $this->secret('Your password'),
            ]));

            $roleName = $this->choice('What is your role?', $this->roles, 1);
           
            $role = $this->findOrCreateRole($roleName);
            $role->users()->save($user);

            $this->info('User created successfully...');
        } catch (\Exception $exception) {
            $this->error('Error occurred when create user...' . $exception->getMessage());
        }
    }

    protected function registerInstallModuleService()
    {
        $data = [
            'alias' => 'enzaime-core',
        ];

        $cmsVersion = get_cms_version();

        $baseCore = $this->coreModuleRepository->findWhere($data);
        if (!$baseCore) {
            $this->coreModuleRepository->create(array_merge($data, [
                'installed_version' => $cmsVersion,
            ]));
        } else {
            $this->coreModuleRepository->update($baseCore, [
                'installed_version' => get_cms_version(),
            ]);
        }

        $modules = get_core_module()->where('namespace', '!=', 'Enzaime\Base');
        $corePackages = get_composer_modules();
        foreach ($modules as $module) {
            $namespace = str_replace('\\\\', '\\', $module['namespace'] . '\Providers\InstallModuleServiceProvider');
            if (class_exists($namespace)) {
                $this->app->register($namespace);
            }
            $data = [
                'alias' => $module['alias'],
            ];
            if (isset($module['repos'])) {
                $currentPackage = $corePackages->where('name', '=', $module['repos'])->first();
            
                $data['installed_version'] = isset($module['version']) ? $module['version'] : $currentPackage['version'];
            }
            $coreModule = $this->coreModuleRepository->findWhere([
                'alias' => $module['alias'],
            ]);
            $this->coreModuleRepository->createOrUpdate($coreModule, $data);
        }
        \Artisan::call('vendor:publish', [
            '--tag' => 'enzaime-public-assets',
            '--force' => true,
        ]);
        \Artisan::call('cache:clear');
    }

    /**
     * @return string
     */
    protected function createEnv()
    {
        $env = $this->files->exists('.env') ? $this->files->get('.env') : $this->files->get('.env.example');
        $env = $env ?: '';
        $this->files->put('.env', $env);
        
        return $env;
    }
}
