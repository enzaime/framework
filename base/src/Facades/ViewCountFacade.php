<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Support\ViewCount;

class ViewCountFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ViewCount::class;
    }
}
