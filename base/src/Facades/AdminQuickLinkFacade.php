<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Support\AdminQuickLink;

class AdminQuickLinkFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return AdminQuickLink::class;
    }
}
