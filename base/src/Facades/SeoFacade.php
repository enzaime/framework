<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Support\SEO;
use Enzaime\Base\Support\ViewCount;

class SeoFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return SEO::class;
    }
}
