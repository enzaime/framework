<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;

class BreadcrumbsFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return \Enzaime\Base\Support\Breadcrumbs::class;
    }
}
