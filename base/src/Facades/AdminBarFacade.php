<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Support\AdminBar;

class AdminBarFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor() : string
    {
        return AdminBar::class;
    }
}
