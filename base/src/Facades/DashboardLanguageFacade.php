<?php namespace Enzaime\Base\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\Support\DashboardLanguage;

/**
 * @method static DashboardLanguage setDashboardLanguage(string $slug)
 * @method static DashboardLanguage getDashboardLanguage(string $default = null)
 */
class DashboardLanguageFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return DashboardLanguage::class;
    }
}
