<?php namespace Enzaime\Base\Repositories;

use Enzaime\Base\Models\Contracts\ViewTrackerModelContract;
use Enzaime\Base\Models\ViewTracker;
use Enzaime\Base\Repositories\Contracts\ViewTrackerRepositoryContract;
use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;

class ViewTrackerRepository extends EloquentBaseRepository implements ViewTrackerRepositoryContract
{
    /**
     * @param string $entityName
     * @param string $entityId
     * @return int
     */
    public function increase($entityName, $entityId)
    {
        $viewTracker = $this->findWhereOrCreate([
            'entity' => $entityName,
            'entity_id' => $entityId,
        ]);

        $this->update($viewTracker, [
            'count' => $viewTracker->count + 1
        ]);
        return $viewTracker->count + 1;
    }
}
