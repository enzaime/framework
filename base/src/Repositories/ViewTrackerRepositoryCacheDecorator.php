<?php namespace Enzaime\Base\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepositoryCacheDecorator;

use Enzaime\Base\Models\Contracts\ViewTrackerModelContract;
use Enzaime\Base\Repositories\Contracts\ViewTrackerRepositoryContract;

class ViewTrackerRepositoryCacheDecorator extends EloquentBaseRepositoryCacheDecorator  implements ViewTrackerRepositoryContract
{
    /**
     * @param string $entityName
     * @param string $entityId
     * @return mixed
     */
    public function increase($entityName, $entityId)
    {
        return $this->afterUpdate(__FUNCTION__, func_get_args());
    }
}
