<?php namespace Enzaime\Base\Support;

use Enzaime\Base\Repositories\Contracts\ViewTrackerRepositoryContract;
use Enzaime\Base\Repositories\ViewTrackerRepository;

class ViewCount
{
    /**
     * @var ViewTrackerRepositoryContract|ViewTrackerRepository
     */
    protected $repository;

    /**
     * ViewCount constructor.
     * @param ViewTrackerRepository $repository
     */
    public function __construct(ViewTrackerRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $entityName
     * @param $entityId
     * @return int
     */
    public function increase($entityName, $entityId)
    {
        return $this->repository->increase($entityName, $entityId);
    }
}
