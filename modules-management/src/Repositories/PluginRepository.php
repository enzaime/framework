<?php namespace Enzaime\Base\ModulesManagement\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepository;
use Enzaime\Base\ModulesManagement\Repositories\Contracts\PluginRepositoryContract;

class PluginRepository extends EloquentBaseRepository implements PluginRepositoryContract
{
    /**
     * @param $alias
     * @return mixed|null
     */
    public function getByAlias($alias)
    {
        return $this->model->where('alias', '=', $alias)->first();
    }
}
