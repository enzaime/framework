<?php namespace Enzaime\Base\ModulesManagement\Repositories;

use Enzaime\Base\Repositories\Eloquent\EloquentBaseRepositoryCacheDecorator;

use Enzaime\Base\ModulesManagement\Repositories\Contracts\PluginRepositoryContract;

class PluginRepositoryCacheDecorator extends EloquentBaseRepositoryCacheDecorator implements PluginRepositoryContract
{
    /**
     * @param $alias
     * @return mixed|null
     */
    public function getByAlias($alias)
    {
        return $this->model->where('alias', '=', $alias)->first();
    }
}
