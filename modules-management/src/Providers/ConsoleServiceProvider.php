<?php namespace Enzaime\Base\ModulesManagement\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->generatorCommands();
        $this->otherCommands();
    }

    protected function generatorCommands()
    {
        $this->commands([
            /**
             * Core
             */
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeModule::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeProvider::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeController::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeMiddleware::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeRequest::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeModel::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeRepository::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeFacade::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeService::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeSupport::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeView::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeDataTable::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeCriteria::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeAction::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeMail::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeViewComposer::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeHook::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeSeeder::class,
            /**
             * Plugin
             */
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeProvider::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeController::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeMiddleware::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeRequest::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeModel::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeRepository::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeFacade::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeService::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeSupport::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeView::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeDataTable::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeCriteria::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeAction::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeMail::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeViewComposer::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeHook::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Plugin\MakeSeeder::class,
            /**
             * Theme
             */
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeProvider::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeController::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeMiddleware::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeRequest::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeModel::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeRepository::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeFacade::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeService::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeSupport::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeView::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeDataTable::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeCriteria::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeAction::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeMail::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeViewComposer::class,
            \Enzaime\Base\ModulesManagement\Console\Generators\Theme\MakeHook::class,
        ]);
    }

    protected function otherCommands()
    {
        $this->commands([
            \Enzaime\Base\ModulesManagement\Console\Commands\InstallPluginCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\UpdatePluginCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\UninstallPluginCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\DisablePluginCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\EnablePluginCommand::class,

            \Enzaime\Base\ModulesManagement\Console\Commands\UpdateCoreModuleCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\CoreVendorPublishCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\PluginVendorPublishCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\ThemeVendorPublishCommand::class,

            \Enzaime\Base\ModulesManagement\Console\Commands\ExportCoreModuleCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\GetAllCoreModulesCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\GetAllPluginsCommand::class,
            \Enzaime\Base\ModulesManagement\Console\Commands\SeedPluginCommand::class,
        ]);
    }
}
