<?php namespace Enzaime\Base\ModulesManagement\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\ModulesManagement\Models\CoreModules;
use Enzaime\Base\ModulesManagement\Models\Plugins;
use Enzaime\Base\ModulesManagement\Repositories\Contracts\CoreModuleRepositoryContract;
use Enzaime\Base\ModulesManagement\Repositories\Contracts\PluginRepositoryContract;
use Enzaime\Base\ModulesManagement\Repositories\CoreModuleRepository;
use Enzaime\Base\ModulesManagement\Repositories\CoreModuleRepositoryCacheDecorator;
use Enzaime\Base\ModulesManagement\Repositories\PluginRepository;
use Enzaime\Base\ModulesManagement\Repositories\PluginRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PluginRepositoryContract::class, function () {
            $repository = new PluginRepository(new Plugins());

            if (config('enzaime-caching.repository.enabled')) {
                return new PluginRepositoryCacheDecorator($repository);
            }

            return $repository;
        });

        $this->app->bind(CoreModuleRepositoryContract::class, function () {
            $repository = new CoreModuleRepository(new CoreModules());

            if (config('enzaime-caching.repository.enabled')) {
                return new CoreModuleRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
