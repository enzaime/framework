<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Plugin;

use Enzaime\Base\ModulesManagement\Console\Generators\PluginGeneratorTrait;

class MakeSeeder extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeSeeder
{
    use PluginGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:make:seeder
    	{alias : The alias of the module}
    	{name : Seeder class name}';
}
