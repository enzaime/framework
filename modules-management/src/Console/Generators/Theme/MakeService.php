<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Theme;

use Enzaime\Base\ModulesManagement\Console\Generators\ThemeGeneratorTrait;

class MakeService extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeService
{
    use ThemeGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make:service
    	{name : The class name}';
}
