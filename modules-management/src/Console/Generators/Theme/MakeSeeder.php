<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Theme;

use Enzaime\Base\ModulesManagement\Console\Generators\PluginGeneratorTrait;

class MakeSeeder extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeSeeder
{
    use PluginGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make:seeder
    	{alias : The alias of the module}
        {name : Seeder class name}';
    
    protected function getStub()
    {
        return __DIR__ . '/../../../../resources/stubs/seeds/seeder.stub';
    }
}
