<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Theme;

use Enzaime\Base\ModulesManagement\Console\Generators\ThemeGeneratorTrait;

class MakeProvider extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeProvider
{
    use ThemeGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make:provider
    	{name : The class name}';
}
