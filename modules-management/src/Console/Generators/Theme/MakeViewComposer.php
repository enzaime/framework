<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Theme;

use Enzaime\Base\ModulesManagement\Console\Generators\ThemeGeneratorTrait;

class MakeViewComposer extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeViewComposer
{
    use ThemeGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make:composer
    	{name : The class name}';
}
