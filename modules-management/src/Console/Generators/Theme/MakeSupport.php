<?php namespace Enzaime\Base\ModulesManagement\Console\Generators\Theme;

use Enzaime\Base\ModulesManagement\Console\Generators\ThemeGeneratorTrait;

class MakeSupport extends \Enzaime\Base\ModulesManagement\Console\Generators\Core\MakeSupport
{
    use ThemeGeneratorTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make:support
    	{name : The class name}';
}
