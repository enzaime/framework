<?php

namespace Enzaime\Base\ModulesManagement\Console\Commands;

use Illuminate\Console\Command;
// use Illuminate\Support\ClassLoader;
use Composer\Autoload\ClassLoader;
use Enzaime\Base\ModulesManagement\Console\Generators\PluginGeneratorTrait;

class SeedPluginCommand extends Command
{
    use PluginGeneratorTrait;

    protected $moduleInformation;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'plugin:db:seed {alias} {--database=} {--class=} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset and re-run all migrations';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {            
        $this->loadSeederClass();
        $this->runSeeder();
    }

    /**
     * Load classes from plugin's database/seeds directory
     * 
     * @return void
     */

    protected function loadSeederClass()
    {
        $path =  $this->getPath();
        $seederClasses = \File::glob($path . '*.php');
        foreach ($seederClasses as $className) {
            require_once $className;
        }
    }

    /**
     * Get module information by key
     * @param $key
     * @return array|mixed
     */  
    protected function getModuleInfo($key = null)
    {
        if (!$this->moduleInformation) {
            $this->getCurrentModule();
        }
        if (!$key) {
            return $this->moduleInformation;
        }
        return array_get($this->moduleInformation, $key, null);
    }

    /**
     * Get the path of seeder classes
     * 
     * @return string
     */
    protected function getPath()
    {
        $path = $this->getModuleInfo('module-path') . 'database/seeds/';

        return $path;
    }

    /**
     * Run the database seeder command.
     *
     * @param  string  $database
     * @return void
     */
    protected function runSeeder()
    {
        $database = $this->option('database');
        
        $alias = $this->argument('alias');

        $className =  $this->option('class') ?: studly_case(preg_replace('/\-/', '_', $alias)) . 'TableSeeder';
        
        $this->call('db:seed', [
            '--database' => $database,
            '--class' => $className,
            '--force' => $this->option('force'),
        ]);
    }
}