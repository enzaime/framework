<?php namespace Enzaime\Base\ModulesManagement\Console\Commands;

use Illuminate\Console\Command;
use Enzaime\Base\ModulesManagement\Actions\UpdatePluginAction;

class UpdatePluginCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:update {alias}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Enzaime plugin';

    /**
     * @param UpdatePluginAction $action
     */
    public function handle(UpdatePluginAction $action)
    {
        $result = $action->run($this->argument('alias'));

        if($result['error']) {
            foreach ($result['messages'] as $message) {
                $this->error($message);
            }
        } else {
            foreach ($result['messages'] as $message) {
                $this->info($message);
            }
        }
    }
}
