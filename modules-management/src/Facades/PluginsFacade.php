<?php namespace Enzaime\Base\ModulesManagement\Facades;

use Illuminate\Support\Facades\Facade;
use Enzaime\Base\ModulesManagement\Support\PluginsSupport;

class PluginsFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return PluginsSupport::class;
    }
}
