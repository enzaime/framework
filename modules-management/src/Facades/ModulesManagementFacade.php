<?php namespace Enzaime\Base\ModulesManagement\Facades;

use Illuminate\Support\Facades\Facade;

class ModulesManagementFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Enzaime\Base\ModulesManagement\Support\ModulesManagement::class;
    }
}
