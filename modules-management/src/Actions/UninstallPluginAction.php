<?php namespace Enzaime\Base\ModulesManagement\Actions;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Enzaime\Base\Actions\AbstractAction;

class UninstallPluginAction extends AbstractAction
{
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    protected $app;

    public function __construct()
    {
        $this->app = app();
    }

    public function run($alias)
    {
        do_action(ENZAIME_PLUGIN_BEFORE_UNINSTALL, $alias);

        DB::beginTransaction();

        $module = get_plugin($alias);

        if (!$module) {
            return $this->error('Plugin not exists');
        }

        $namespace = str_replace('\\\\', '\\', array_get($module, 'namespace', '') . '\Providers\UninstallModuleServiceProvider');

        if (class_exists($namespace)) {
            $this->app->register($namespace);
        }

        enzaime_plugins()
            ->savePlugin($module, [
                'installed' => false,
                'installed_version' => '',
            ]);

        DB::commit();

        enzaime_plugins()->modifyComposerAutoload($alias, true);

        Artisan::call('cache:clear');

        do_action(ENZAIME_PLUGIN_AFTER_UNINSTALL, $alias);

        return $this->success('Your plugin has been uninstalled');
    }
}
