<?php namespace Enzaime\Base\ModulesManagement\Actions;

use Enzaime\Base\Actions\AbstractAction;

class DisablePluginByUserAction extends AbstractAction
{
    public function run($alias)
    {
        do_action(ENZAIME_PLUGIN_BEFORE_DISABLE_BY_USER, $alias);

        $module = get_plugin($alias);

        if (!$module) {
            return $this->error('Plugin not exists');
        }

        $checkRelatedModules = check_module_require($module);
        if ($checkRelatedModules['error']) {
            $messages = [];
            foreach ($checkRelatedModules['messages'] as $message) {
                $messages[] = $message;
            }
            return $this->error($messages);
        }

        $subdomain = subdomain();        

        enzaime_plugins()->disableModuleBySubdomain($subdomain, $alias);

        do_action(ENZAIME_PLUGIN_DISABLED_BY_USER, $alias);


        return $this->success('Your plugin has been disabled');
    }
}
