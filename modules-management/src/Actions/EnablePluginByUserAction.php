<?php namespace Enzaime\Base\ModulesManagement\Actions;

use Enzaime\Base\Actions\AbstractAction;

class EnablePluginByUserAction extends AbstractAction
{
    /**
     * @param $alias
     * @return array
     */
    public function run($alias)
    {
        do_action(ENZAIME_PLUGIN_BEFORE_ENABLE_BY_USER, $alias);

        $module = get_plugin($alias);

        if (!$module) {
            return $this->error('Plugin not exists');
        }

        $checkRelatedModules = check_module_require($module);
        if ($checkRelatedModules['error']) {
            $messages = [];
            foreach ($checkRelatedModules['messages'] as $message) {
                $messages[] = $message;
            }
            return $this->error($messages);
        }

        $subdomain = subdomain();

        enzaime_plugins()->enableModuleBySubdomain($subdomain, $alias);

        do_action(ENZAIME_PLUGIN_ENABLED_BY_USER, $alias);

        modules_management()->refreshComposerAutoload();

        return $this->success('Your plugin has been enabled');
    }
}
