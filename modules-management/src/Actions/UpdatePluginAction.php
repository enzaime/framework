<?php namespace Enzaime\Base\ModulesManagement\Actions;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Enzaime\Base\Actions\AbstractAction;

class UpdatePluginAction extends AbstractAction
{
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    protected $app;

    public function __construct()
    {
        $this->app = app();
    }

    public function run($alias)
    {
        do_action(ENZAIME_PLUGIN_BEFORE_UPDATE, $alias);

        DB::beginTransaction();

        $module = get_plugin($alias);

        if (!$module) {
            return $this->error('Plugin not exists');
        }

        if (array_get($module, 'version') === array_get($module, 'installed_version')) {
            return $this->error("Plugin " . $alias . " already up to date");
        }

        $namespace = str_replace('\\\\', '\\', array_get($module, 'namespace', '') . '\Providers\UpdateModuleServiceProvider');
        if (class_exists($namespace)) {
            $this->app->register($namespace);
        }

        enzaime_plugins()
            ->savePlugin($module, [
                'installed_version' => array_get($module, 'version'),
            ]);

        $moduleProvider = str_replace('\\\\', '\\', array_get($module, 'namespace', '') . '\Providers\ModuleProvider');

        DB::commit();

        Artisan::call('vendor:publish', [
            '--provider' => $moduleProvider,
            '--tag' => 'enzaime-public-assets',
            '--force' => true
        ]);

        Artisan::call('cache:clear');

        do_action(ENZAIME_PLUGIN_UPDATED, $alias);

        return $this->success('Your plugin has been updated');
    }
}
