<?php namespace Enzaime\Base\ModulesManagement\Models;

use Enzaime\Base\ModulesManagement\Models\Contracts\PluginsModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class Plugins extends BaseModel implements PluginsModelContract
{
    protected $table = 'plugins';

    protected $primaryKey = 'id';
    
    protected $subdomain = false;

    protected $fillable = [
        'alias',
        'installed_version',
        'enabled',
        'installed',
    ];

    public $timestamps = true;

    public function setEnabledAttribute($value)
    {
        $this->attributes['enabled'] = (int)!!$value;
    }

    public function setInstalledAttribute($value)
    {
        $this->attributes['installed'] = (int)!!$value;
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'user_plugin');
    }
}
