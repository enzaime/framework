<?php namespace Enzaime\Base\ModulesManagement\Models;

use Enzaime\Base\ModulesManagement\Models\Contracts\CoreModulesModelContract;
use Enzaime\Base\Models\EloquentBase as BaseModel;

class CoreModules extends BaseModel implements CoreModulesModelContract
{
    protected $table = 'core_modules';

    protected $primaryKey = 'id';

    protected $fillable = [
        'alias',
        'installed_version',
    ];

    public $timestamps = true;
}
