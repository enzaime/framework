<?php namespace Enzaime\Base\ModulesManagement\Http\Controllers;

use Enzaime\Base\Users\Repositories\UserRepository;
use Enzaime\Base\Http\Controllers\BaseAdminController;
use Enzaime\Base\ModulesManagement\Actions\EnablePluginAction;
use Enzaime\Base\ModulesManagement\Actions\UpdatePluginAction;
use Enzaime\Base\ModulesManagement\Actions\DisablePluginAction;
use Enzaime\Base\ModulesManagement\Actions\InstallPluginAction;
use Enzaime\Base\ModulesManagement\Actions\UninstallPluginAction;
use Enzaime\Base\ModulesManagement\Actions\EnablePluginByUserAction;
use Enzaime\Base\Users\Repositories\Contracts\UserRepositoryContract;
use Enzaime\Base\ModulesManagement\Actions\DisablePluginByUserAction;
use Enzaime\Base\ModulesManagement\Http\DataTables\PluginsListDataTable;

class PluginsController extends BaseAdminController
{
    /**
     * @var string
     */
    protected $module = ENZAIME_MODULES_MANAGEMENT;

    /**
     * @var string
     */
    protected $dashboardMenuId = 'enzaime-plugins';

    

    /**
     * Get index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(PluginsListDataTable $dataTable)
    {
        $this->breadcrumbs->addLink(trans($this->module . '::base.plugins'));

        $this->setPageTitle(trans($this->module . '::base.plugins'));

        $this->getDashboardMenu($this->dashboardMenuId);

        $this->dis['dataTable'] = $dataTable->run();

        return $this->viewAdmin('plugins-list');
        return do_filter(BASE_FILTER_CONTROLLER, $this, ENZAIME_PLUGINS, 'index.get', $dataTable)->viewAdmin('plugins-list');
    }

    /**
     * Set data for DataTable plugin
     * @param PluginsListDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse
     */
    public function postListing(PluginsListDataTable $dataTable)
    {
        return do_filter(BASE_FILTER_CONTROLLER, $dataTable, ENZAIME_PLUGINS, 'index.post', $this);
    }

    /**
     * @param $alias
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangeStatus($alias, $status)
    {
        switch ((bool)$status) {
            case true:
                $result = app(EnablePluginByUserAction::class)->run($alias);
                break;
            default:
                $result = app(DisablePluginByUserAction::class)->run($alias);
                break;
        }
        // dd($result);

        return response()->json($result, $result['response_code']);
    }

    /**
     * @param InstallPluginAction $action
     * @param $alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function postInstall(InstallPluginAction $action, $alias)
    {
        $result = $action->run($alias);

        return response()->json($result, $result['response_code']);
    }

    /**
     * @param UpdatePluginAction $action
     * @param $alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUpdate(UpdatePluginAction $action, $alias)
    {
        $result = $action->run($alias);

        return response()->json($result, $result['response_code']);
    }

    /**
     * @param UninstallPluginAction $action
     * @param $alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUninstall(UninstallPluginAction $action, $alias)
    {
        $result = $action->run($alias);

        return response()->json($result, $result['response_code']);
    }
}
