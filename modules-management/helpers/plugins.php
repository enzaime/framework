<?php

use Illuminate\Support\Collection;
use Enzaime\Base\ModulesManagement\Facades\PluginsFacade;

if (!function_exists('enzaime_plugins')) {
    /**
     * @return \Enzaime\Base\ModulesManagement\Support\PluginsSupport
     */
    function enzaime_plugins()
    {
        return PluginsFacade::getFacadeRoot();
    }
}

if (!function_exists('get_plugin')) {
    /**
     * @param string
     * @return Collection|array
     */
    function get_plugin($alias = null)
    {
        if ($alias) {
            return enzaime_plugins()->findByAlias($alias);
        }
        return enzaime_plugins()->getAllPlugins();
    }
}

if (!function_exists('save_plugin_information')) {
    /**
     * @param $alias
     * @param array $data
     * @return bool
     */
    function save_plugin_information($alias, array $data)
    {
        return enzaime_plugins()->savePlugin($alias, $data);
    }
}
