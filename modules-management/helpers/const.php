<?php

if (!defined('ENZAIME_MODULES_MANAGEMENT')) {
    define('ENZAIME_MODULES_MANAGEMENT', 'enzaime-modules-management');
}

if (!defined('ENZAIME_CORE_MODULES')) {
    define('ENZAIME_CORE_MODULES', 'enzaime-modules-management.core-modules');
}

if (!defined('ENZAIME_PLUGINS')) {
    define('ENZAIME_PLUGINS', 'enzaime-modules-management.plugins');
}

if (!defined('ENZAIME_CORE_BEFORE_UPDATE')) {
    define('ENZAIME_CORE_BEFORE_UPDATE', 'enzaime-modules-management.core-before-update');
}

if (!defined('ENZAIME_CORE_UPDATED')) {
    define('ENZAIME_CORE_UPDATED', 'enzaime-modules-management.core-updated');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_INSTALL')) {
    define('ENZAIME_PLUGIN_BEFORE_INSTALL', 'enzaime-modules-management.plugin-before-install');
}

if (!defined('ENZAIME_PLUGIN_AFTER_INSTALL')) {
    define('ENZAIME_PLUGIN_AFTER_INSTALL', 'enzaime-modules-management.plugin-after-install');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_UPDATE')) {
    define('ENZAIME_PLUGIN_BEFORE_UPDATE', 'enzaime-modules-management.plugin-before-update');
}

if (!defined('ENZAIME_PLUGIN_UPDATED')) {
    define('ENZAIME_PLUGIN_UPDATED', 'enzaime-modules-management.plugin-updated');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_ENABLE')) {
    define('ENZAIME_PLUGIN_BEFORE_ENABLE', 'enzaime-modules-management.plugin-before-enable');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_ENABLE_BY_USER')) {
    define('ENZAIME_PLUGIN_BEFORE_ENABLE_BY_USER', 'enzaime-modules-management.plugin-before-enable-by-user');
}

if (!defined('ENZAIME_PLUGIN_ENABLED_BY_USER')) {
    define('ENZAIME_PLUGIN_ENABLED_BY_USER', 'enzaime-modules-management.plugin-enabled-by-user');
}


if (!defined('ENZAIME_PLUGIN_ENABLED')) {
    define('ENZAIME_PLUGIN_ENABLED', 'enzaime-modules-management.plugin-enabled');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_DISABLE_BY_USER')) {
    define('ENZAIME_PLUGIN_BEFORE_DISABLE_BY_USER', 'enzaime-modules-management.plugin-before-disable-by-user');
}

if (!defined('ENZAIME_PLUGIN_DISABLED_BY_USER')) {
    define('ENZAIME_PLUGIN_DISABLED_BY_USER', 'enzaime-modules-management.plugin-disabled-by-user');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_DISABLE')) {
    define('ENZAIME_PLUGIN_BEFORE_DISABLE', 'enzaime-modules-management.plugin-before-disable');
}

if (!defined('ENZAIME_PLUGIN_DISABLED')) {
    define('ENZAIME_PLUGIN_DISABLED', 'enzaime-modules-management.plugin-disabled');
}

if (!defined('ENZAIME_PLUGIN_BEFORE_UNINSTALL')) {
    define('ENZAIME_PLUGIN_BEFORE_UNINSTALL', 'enzaime-modules-management.plugin-before-uninstall');
}

if (!defined('ENZAIME_PLUGIN_AFTER_UNINSTALL')) {
    define('ENZAIME_PLUGIN_AFTER_UNINSTALL', 'enzaime-modules-management.plugin-after-uninstall');
}
