<?php

if (!function_exists('enzaime_plugins_path')) {
    /**
     * @param string $path
     * @return string
     */
    function enzaime_plugins_path($path = '')
    {
        return base_path('plugins') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('enzaime_core_path')) {
    /**
     * @param string $path
     * @return string
     */
    function enzaime_core_path($path = '')
    {
        // return base_path('core') . ($path ? DIRECTORY_SEPARATOR . $path : $path);

        return realpath(__DIR__ . '/../../' );
    }
}

if (!function_exists('modules_management')) {
    /**
     * @return \Enzaime\Base\ModulesManagement\Support\ModulesManagement
     */
    function modules_management()
    {
        return \Enzaime\Base\ModulesManagement\Facades\ModulesManagementFacade::getFacadeRoot();
    }
}
