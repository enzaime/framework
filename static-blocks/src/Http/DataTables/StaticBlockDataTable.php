<?php namespace Enzaime\Base\StaticBlocks\Http\DataTables;

use Enzaime\Base\Http\DataTables\AbstractDataTables;
use Enzaime\Base\StaticBlocks\Actions\DeleteStaticBlockAction;
use Enzaime\Base\StaticBlocks\Actions\UpdateStaticBlockAction;
use Enzaime\Base\StaticBlocks\Models\StaticBlock;

class StaticBlockDataTable extends AbstractDataTables
{
    protected $model;

    protected $screenName = ENZAIME_STATIC_BLOCKS;

    public function __construct()
    {
        $this->model = do_filter(
            FRONT_FILTER_DATA_TABLES_MODEL,
            StaticBlock::select(['id', 'title', 'created_at', 'slug', 'status']),
            $this->screenName
        );
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'title' => [
                'title' => trans('enzaime-core::datatables.heading.title'),
                'width' => '25%',
            ],
            'status' => [
                'title' => trans('enzaime-core::datatables.heading.status'),
                'width' => '10%',
            ],
            'shortcode' => [
                'title' => 'Shortcode',
                'width' => '20%',
            ],
            'created_at' => [
                'title' => trans('enzaime-core::datatables.heading.created_at'),
                'width' => '15%',
            ],
            'actions' => [
                'title' => trans('enzaime-core::datatables.heading.actions'),
                'width' => '30%',
            ],
        ];
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            ['data' => 'id', 'name' => 'id', 'searchable' => false, 'orderable' => false],
            ['data' => 'title', 'name' => 'title'],
            ['data' => 'status', 'name' => 'status'],
            ['data' => 'shortcode', 'name' => 'shortcode'],
            ['data' => 'created_at', 'name' => 'created_at'],
            ['data' => 'actions', 'name' => 'actions', 'searchable' => false, 'orderable' => false],
        ];
    }

    /**
     * @return string
     */
    public function run(): string
    {
        $this->setAjaxUrl(route('admin::static-blocks.index.post'), 'POST');

        $this
            ->addFilter(1, form()->text('title', '', [
                'class' => 'form-control form-filter input-sm',
                'placeholder' => trans('enzaime-core::datatables.search') . '...',
            ]));

        $this->withGroupActions([
            '' => trans('enzaime-core::datatables.select') . '...',
            'deleted' => trans('enzaime-core::datatables.delete_these_items'),
            1 => trans('enzaime-core::datatables.active_these_items'),
            0 => trans('enzaime-core::datatables.disable_these_items'),
        ]);

        return $this->view();
    }

    /**
     * @return mixed
     */
    protected function fetchDataForAjax()
    {
        return enzaime_datatable()->of($this->model)
            ->rawColumns(['actions', 'shortcode'])
            ->editColumn('id', function ($item) {
                return form()->customCheckbox([['id[]', $item->id]]);
            })
            ->editColumn('status', function ($item) {
                $status = $item->status ? 'activated' : 'disabled';
                return html()->label(trans('enzaime-core::base.status.' . $status), $status);
            })
            ->addColumn('shortcode', function ($item) {
                return form()->text('', $item->shortcode_alias, [
                    'class' => 'form-control',
                    'readonly' => 'readonly'
                ]);
            })
            ->addColumn('actions', function ($item) {
                /*Edit link*/
                $activeLink = route('admin::static-blocks.update-status.post', ['id' => $item->id, 'status' => 1]);
                $disableLink = route('admin::static-blocks.update-status.post', ['id' => $item->id, 'status' => 0]);
                $deleteLink = route('admin::static-blocks.delete.post', ['id' => $item->id]);

                /*Buttons*/
                $editBtn = link_to(route('admin::static-blocks.edit.get', ['id' => $item->id]), trans('enzaime-core::datatables.edit'), ['class' => 'btn btn-sm btn-outline green']);
                $activeBtn = ($item->status != 1) ? form()->button(trans('enzaime-core::datatables.active'), [
                    'title' => trans('enzaime-core::datatables.active_this_item'),
                    'data-ajax' => $activeLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline blue btn-sm ajax-link',
                    'type' => 'button',
                ]) : '';
                $disableBtn = ($item->status != 0) ? form()->button(trans('enzaime-core::datatables.disable'), [
                    'title' => trans('enzaime-core::datatables.disable_this_item'),
                    'data-ajax' => $disableLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline yellow-lemon btn-sm ajax-link',
                    'type' => 'button',
                ]) : '';
                $deleteBtn = form()->button(trans('enzaime-core::datatables.delete'), [
                    'title' => trans('enzaime-core::datatables.delete_this_item'),
                    'data-ajax' => $deleteLink,
                    'data-method' => 'POST',
                    'data-toggle' => 'confirmation',
                    'class' => 'btn btn-outline red-sunglo btn-sm ajax-link',
                    'type' => 'button',
                ]);

                return $editBtn . $activeBtn . $disableBtn . $deleteBtn;
            });
    }

    /**
     * Handle group actions
     * @return array
     */
    protected function groupAction(): array
    {
        $request = request();

        $data = [];
        if ($request->input('customActionType', null) === 'group_action') {
            if (!has_permissions(get_current_logged_user(), ['edit-static-blocks'])) {
                return [
                    'customActionMessage' => trans('enzaime-acl::base.do_not_have_permission'),
                    'customActionStatus' => 'danger',
                ];
            }

            $ids = (array)$request->input('id', []);
            $actionValue = $request->input('customActionValue');

            switch ($actionValue) {
                case 'deleted':
                    if (!has_permissions(get_current_logged_user(), ['delete-static-blocks'])) {
                        return [
                            'customActionMessage' => trans('enzaime-acl::base.do_not_have_permission'),
                            'customActionStatus' => 'danger',
                        ];
                    }
                    /**
                     * Delete items
                     */
                    $action = app(DeleteStaticBlockAction::class);
                    foreach ($ids as $id) {
                        $action->run($id);
                    }
                    break;
                case 1:
                case 0:
                    $action = app(UpdateStaticBlockAction::class);

                    foreach ($ids as $id) {
                        $action->run($id, [
                            'status' => $actionValue,
                        ]);
                    }
                    break;
                default:
                    return [
                        'customActionMessage' => trans('enzaime-core::errors.' . \Constants::METHOD_NOT_ALLOWED . '.message'),
                        'customActionStatus' => 'danger'
                    ];
                    break;
            }
            $data['customActionMessage'] = trans('enzaime-core::base.form.request_completed');
            $data['customActionStatus'] = 'success';
        }
        return $data;
    }
}
