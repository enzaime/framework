<?php namespace Enzaime\Base\StaticBlocks\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\StaticBlocks\Repositories\Contracts\StaticBlockRepositoryContract;
use Enzaime\Base\StaticBlocks\Repositories\StaticBlockRepository;

class UpdateStaticBlockAction extends AbstractAction
{
    /**
     * @var StaticBlockRepository
     */
    protected $repository;

    public function __construct(StaticBlockRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @param array $data
     * @return array
     */
    public function run($id, array $data)
    {
        $item = $this->repository->find($id);

        $item = do_filter(BASE_FILTER_BEFORE_UPDATE, $item, ENZAIME_STATIC_BLOCKS, 'edit.post');

        if (!$item) {
            return $this->error(trans('enzaime-core::base.form.item_not_exists'));
        }

        $data['updated_by'] = get_current_logged_user_id();

        $result = $this->repository->updateStaticBlock($item, $data);

        do_action(BASE_ACTION_AFTER_UPDATE, ENZAIME_STATIC_BLOCKS, $id, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
