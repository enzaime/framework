<?php namespace Enzaime\Base\StaticBlocks\Actions;

use Enzaime\Base\Actions\AbstractAction;
use Enzaime\Base\StaticBlocks\Repositories\Contracts\StaticBlockRepositoryContract;
use Enzaime\Base\StaticBlocks\Repositories\StaticBlockRepository;

class DeleteStaticBlockAction extends AbstractAction
{
    /**
     * @var StaticBlockRepository
     */
    protected $repository;

    public function __construct(StaticBlockRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return array
     */
    public function run($id)
    {
        $id = do_filter(BASE_FILTER_BEFORE_DELETE, $id, ENZAIME_STATIC_BLOCKS);

        $result = $this->repository->deleteStaticBlock($id);

        do_action(BASE_ACTION_AFTER_DELETE, ENZAIME_STATIC_BLOCKS, $id, $result);

        if (!$result) {
            return $this->error();
        }

        return $this->success(null, [
            'id' => $result,
        ]);
    }
}
