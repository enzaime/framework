<?php namespace Enzaime\Base\StaticBlocks\Support;

use Enzaime\Base\StaticBlocks\Repositories\Contracts\StaticBlockRepositoryContract;
use Enzaime\Base\StaticBlocks\Repositories\StaticBlockRepository;

class StaticBlockShortcodeRenderer
{
    /**
     * @var StaticBlockRepository
     */
    protected $repository;

    public function __construct(StaticBlockRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @var \Enzaime\Base\Shortcode\Compilers\Shortcode $shortcode
     * @var string $content
     * @var \Enzaime\Base\Shortcode\Compilers\ShortcodeCompiler $compiler
     * @var string $name
     * @return mixed|string
     */
    public function handle($shortcode, $content, $compiler, $name)
    {
        $block = $this->repository->findWhere([
            'slug' => $shortcode->alias,
        ]);
        if (!$block) {
            return null;
        }

        return $block->content;
    }
}
