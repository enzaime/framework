<?php namespace Enzaime\Base\StaticBlocks\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Enzaime\Base\Events\SessionStarted;

class BootstrapModuleServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Event::listen(SessionStarted::class, function () {
            $this->onSessionStarted();
        });
    }

    /**
     * Register dashboard menus, translations, cms settings
     */
    protected function onSessionStarted()
    {
        dashboard_menu()->registerItem([
            'id' => 'enzaime-static-blocks',
            'priority' => 1.1,
            'parent_id' => null,
            'heading' => null,
            'title' => trans('enzaime-static-blocks::base.admin_menu.title'),
            'font_icon' => 'icon-doc ion',
            'link' => route('admin::static-blocks.index.get'),
            'css_class' => null,
            'permissions' => ['has-permission:view-static-blocks'],
        ]);
    }
}
