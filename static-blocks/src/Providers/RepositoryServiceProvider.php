<?php namespace Enzaime\Base\StaticBlocks\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\StaticBlocks\Models\StaticBlock;
use Enzaime\Base\StaticBlocks\Repositories\Contracts\StaticBlockRepositoryContract;
use Enzaime\Base\StaticBlocks\Repositories\StaticBlockRepository;
use Enzaime\Base\StaticBlocks\Repositories\StaticBlockRepositoryCacheDecorator;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StaticBlockRepositoryContract::class, function () {
            $repository = new StaticBlockRepository(new StaticBlock());

            if (config('enzaime-caching.repository.enabled')) {
                return new StaticBlockRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
