<?php namespace Enzaime\Base\CustomFields\Providers;

use Illuminate\Support\ServiceProvider;
use Enzaime\Base\CustomFields\Repositories\Contracts\CustomFieldRepositoryContract;
use Enzaime\Base\CustomFields\Repositories\Contracts\FieldGroupRepositoryContract;
use Enzaime\Base\CustomFields\Repositories\Contracts\FieldItemRepositoryContract;
use Enzaime\Base\CustomFields\Repositories\CustomFieldRepository;
use Enzaime\Base\CustomFields\Repositories\CustomFieldRepositoryCacheDecorator;
use Enzaime\Base\CustomFields\Repositories\FieldGroupRepository;
use Enzaime\Base\CustomFields\Repositories\FieldItemRepository;
use Enzaime\Base\CustomFields\Models\CustomField;
use Enzaime\Base\CustomFields\Models\FieldGroup;
use Enzaime\Base\CustomFields\Models\FieldItem;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FieldGroupRepositoryContract::class, function () {
            return new FieldGroupRepository(new FieldGroup());
        });
        $this->app->bind(FieldItemRepositoryContract::class, function () {
            return new FieldItemRepository(new FieldItem());
        });
        $this->app->bind(CustomFieldRepositoryContract::class, function () {
            $repository = new CustomFieldRepository(new CustomField());

            if (config('enzaime-caching.repository.enabled')) {
                return new CustomFieldRepositoryCacheDecorator($repository);
            }

            return $repository;
        });
    }
}
